var id;

$(".deleteBuilding").click(function () {
    var r = confirm("Da li ste sigurni da želite da izbrišete ovu zgradu?");
    if (r == true) {
        id = $(this).parent().parent().attr('id');
        $("input[name^=_method]").val("DELETE");
        $("#buildingForm").attr("action", window.location + "/delete/" + id);
        $("#buildingButton").click();
    }
});

$(".photoBuilding").click(function () {
    id = $(this).parent().parent().attr('id');
    $("input[name^=_method]").val("GET");
    $("#buildingForm").attr("action", window.location + "/photos/" + id);
    $("#buildingButton").click();
});

$('#my-date').datetimepicker({
    format: "DD. MMMM YYYY",
    locale: 'sr',
    viewMode: 1,
    minViewMode: 0,
    weekStart: 1,
            pickDate: true,
            pickTime: false,
            startDate: moment({ y: 2009 }),
            endDate: moment().add(50, "y"),
            collapse: true,
            language: "rs",
});
$(window).load(function() {
  // Set main_image feed
    $("#mainImageHolder").append(
            "<img class='thumbnail' src='"+ base() + "public/images/buildings/" + $("#mainImageFeed").val()+"' height='100' width='150' />");
    if($("#mainImageFeed").val() != "default.png") $("#removeMainImage").attr("disabled", false);
  // Set plans feed
    if ($("#plansFeed").val() != 'default.png'){
        $.each($("#plansFeed").val().split("|"), function(index, value){
            $("#plansHolder").append(
                "<img class='thumbnail' src='"+ base() + "public/images/buildings/" + value +"', height='100' width='150' />");
        });
        $("#removePlans").attr("disabled", false);
    }
    else {
        $("#plansHolder").append(
            "<img class='thumbnail' src='"+ base() + "public/images/buildings/" + $("#plansFeed").val()+"' height='100' width='150' />");    
    }
 // Set photos feed
    if ($("#photosFeed").val() != 'default.png'){
        $.each($("#photosFeed").val().split("|"), function(index, value){
            $("#photosHolder").append(
                "<img class='thumbnail' src='"+ base() + "public/images/buildings/" + value +"', height='100' width='150' />");
        });
        $("#removePhotos").attr("disabled", false);
    }
    else {
        $("#photosHolder").append(
            "<img class='thumbnail' src='"+ base() + "public/images/buildings/" + $("#photosFeed").val()+"' height='100' width='150' />");    
    }
});
/* Upload image file click */
$("#uploadMainImage").click(function() {
   $("#uploadMainImageFile").click();
});
$("#uploadPlans").click(function() {
   $("#uploadPlanFiles").click();
});
$("#uploadPhotos").click(function() {
   $("#uploadPhotoFiles").click();
});

/* Upload image */
$(".uploadMainImageFile").on("change", function() {
    var files = !!this.files ? this.files : [];
    if (!files.length || !window.FileReader) return alert("Nije izabrana slika ili nije omogućena podrška"); // no file selected, or no FileReader support

    if (/\.(jpe?g)$/i.test( files[0].name)) { // only jpg file
        $("#mainImageHolder").empty();
        var reader = new FileReader(); // instance of the FileReader
        reader.readAsDataURL(files[0]); // read the local file

        reader.onloadend = function() { // set image data as background of div
            $("#mainImageHolder").append(
                    "<img class='thumbnail' src='"+this.result+"' height='100' width='150' />");
            $("#removeMainImage").attr("disabled", false);
        };
        $("#mainImageCheck").val('changed');
    }
    else {
        return alert(file.name +" nije u JPEG formatu");
    }
});
$("#uploadPlanFiles").on("change", function() {
    var $preview = $("#plansHolder").empty();
    var check = 0;
    if (!this.files.length || !window.FileReader) return alert("Nije izabrana slika ili nije omogućena podrška"); // no file selected, or no FileReader support
    if (this.files) $.each(this.files, function(index, file){
        if (/\.(jpe?g)$/i.test(file.name)){
            var reader = new FileReader();
            $(reader).on("load", function() {
                $preview.append(
                        "<img class='thumbnail' src='"+this.result+"' height='100' width='150' />");
            });
            reader.readAsDataURL(file);
            check = 1;
            $("#plansCheck").val('changed');
        }
        else{
            return alert(file.name +" nije u JPEG formatu");
        }
    });
    if(check) $("#removePlans").attr("disabled", false);
});
$("#uploadPhotoFiles").on("change", function() {
    var $preview = $("#photosHolder").empty();
    var check = 0;
    if (!this.files.length || !window.FileReader) return alert("Nije izabrana slika ili nije omogućena podrška"); // no file selected, or no FileReader support
    if (this.files) $.each(this.files, function(index, file){
        if (/\.(jpe?g)$/i.test(file.name)){
            var reader = new FileReader();
            $(reader).on("load", function() {
                $preview.append(
                        "<img class='thumbnail' src='"+this.result+"' height='100' width='150'/>");
            });
            reader.readAsDataURL(file);
            check = 1;
            $("#photosCheck").val('changed');
        }
        else{
            return alert(file.name +" nije u JPEG formatu");
        }
    });
    if(check) $("#removePhotos").attr("disabled", false);
});

/* Remove image */
$('#removeMainImage').click(function() {
    $("#mainImageHolder").empty();
    $("#mainImageFeed").val('default.png');
    $("#mainImageHolder").append(
            "<img class='thumbnail' src='"+ base() + "public/images/buildings/default.png' height='100' width='150' />");
    $("#removeMainImage").attr("disabled", true);
    $("#uploadMainImageFile").val(null);
});
$('#removePlans').click(function() {
    $("#plansHolder").empty();
    $("#plansFeed").val('default.png');
    $("#plansHolder").append(
            "<img class='thumbnail' src='"+ base() + "public/images/buildings/default.png' height='100' width='150' />");
    $("#removePlans").attr("disabled", true);
    $("#uploadPlanFiles").val(null);
});
$('#removePhotos').click(function() {
    $("#photosHolder").empty();
    $("#photosFeed").val('default.png');
    $("#photosHolder").append(
            "<img class='thumbnail' src='"+ base() + "public/images/buildings/default.png' height='100' width='150' />");
    $("#removePhotos").attr("disabled", true);
    $("#uploadPhotoFiles").val(null);
});
/* Get base url */
function base()
{
    var urlbase = location.host + '/';

    if((location.href.indexOf("localhost") !== -1) || (location.href.indexOf("http://192.168.") !== -1)){
        var positionLocalHost = location.href.indexOf("localhost") + 9;
        urlbase = location.host + '/'+ location.href.substring(positionLocalHost).split('/')[1] +'/';
    }

    return "http://" + urlbase;
}
