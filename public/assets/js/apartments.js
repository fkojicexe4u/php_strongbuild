var id;

$(".deleteApartment").click(function () {
    var r = confirm("Da li ste sigurni da želite da izbrišete ovu zgradu?");
    if (r == true) {
        id = $(this).parent().parent().attr('id');
        $("input[name^=_method]").val("DELETE");
        $("#apartmentForm").attr("action", window.location + "/delete/" + id);
        $("#apartmentButton").click();
    }
});

$(".cloneApartment").click(function () {
    id = $(this).parent().parent().attr('id');
    $("input[name^=_method]").val("GET");
    $("#apartmentForm").attr("action", window.location + "/copy/" + id);
    $("#apartmentButton").click();
});

$(".photoApartment").click(function () {
    id = $(this).parent().parent().attr('id');
    $("input[name^=_method]").val("GET");
    $("#apartmentForm").attr("action", window.location + "/photos/" + id);
    $("#apartmentButton").click();
});

$(".status1Apartment").click(function () {
    id = $(this).parent().parent().attr('id');
    $("input[name^=_method]").val("GET");
    $("#apartmentForm").attr("action", window.location + "/status/" + id + "_1");
    $("#apartmentButton").click();
});

$(".status2Apartment").click(function () {
    id = $(this).parent().parent().attr('id');
    $("input[name^=_method]").val("GET");
    $("#apartmentForm").attr("action", window.location + "/status/" + id + "_2");
    $("#apartmentButton").click();
});

$(".status3Apartment").click(function () {
    id = $(this).parent().parent().attr('id');
    $("input[name^=_method]").val("GET");
    $("#apartmentForm").attr("action", window.location + "/status/" + id + "_3");
    $("#apartmentButton").click();
});

$(window).load(function() {
  // Set plan feed
    $("#planHolder").append(
            "<image src='"+ base() + "public/images/apartments/" + $("#planFeed").val()+"' height='100' width='150' />");
    if($("#planFeed").val() != "default.png") $("#removePlan").attr("disabled", false);
    $("#photo1Holder").append(
            "<image src='"+ base() + "public/images/apartments/" + $("#photo1Feed").val()+"' height='100' width='150' />");
    if($("#photo1Feed").val() != "default.png") $("#removePhoto1").attr("disabled", false);
    $("#photo2Holder").append(
            "<image src='"+ base() + "public/images/apartments/" + $("#photo2Feed").val()+"' height='100' width='150' />");
    if($("#photo2Feed").val() != "default.png") $("#removePhoto2").attr("disabled", false);
    $("#photo3Holder").append(
            "<image src='"+ base() + "public/images/apartments/" + $("#photo3Feed").val()+"' height='100' width='150' />");
    if($("#photo3Feed").val() != "default.png") $("#removePhoto3").attr("disabled", false);
    $("#photo4Holder").append(
            "<image src='"+ base() + "public/images/apartments/" + $("#photo4Feed").val()+"' height='100' width='150' />");
    if($("#photo4Feed").val() != "default.png") $("#removePhoto4").attr("disabled", false);
    $("#photo5Holder").append(
            "<image src='"+ base() + "public/images/apartments/" + $("#photo5Feed").val()+"' height='100' width='150' />");
    if($("#photo5Feed").val() != "default.png") $("#removePhoto5").attr("disabled", false);
});

$("#uploadPlan").click(function() {
   $("#uploadPlanFile").click();
});
$("#uploadPhoto1").click(function() {
   $("#uploadPhoto1File").click();
});
$("#uploadPhoto2").click(function() {
   $("#uploadPhoto2File").click();
});
$("#uploadPhoto3").click(function() {
   $("#uploadPhoto3File").click();
});
$("#uploadPhoto4").click(function() {
   $("#uploadPhoto4File").click();
});
$("#uploadPhoto5").click(function() {
   $("#uploadPhoto5File").click();
});

$(".uploadPlanFile").on("change", function() {
    var files = !!this.files ? this.files : [];
    if (!files.length || !window.FileReader) return alert("Nije izabrana slika ili nije omogućena podrška"); // no file selected, or no FileReader support

    if (/\.(jpe?g)$/i.test( files[0].name)) { // only jpg file
        $("#planHolder").empty();
        var reader = new FileReader(); // instance of the FileReader
        reader.readAsDataURL(files[0]); // read the local file

        reader.onloadend = function() { // set image data as background of div
            $("#planHolder").append($("<img/>", {src:this.result, height:100, width:150}));
            $("#removePlan").attr("disabled", false);
        };
        $("#planCheck").val('changed');
    }
    else {
        return alert(file.name +" nije u JPEG formatu");
    }
});
$(".uploadPhoto1File").on("change", function() {
    var files = !!this.files ? this.files : [];
    if (!files.length || !window.FileReader) return alert("Nije izabrana slika ili nije omogućena podrška"); // no file selected, or no FileReader support

    if (/\.(jpe?g)$/i.test( files[0].name)) { // only jpg file
        $("#photo1Holder").empty();
        var reader = new FileReader(); // instance of the FileReader
        reader.readAsDataURL(files[0]); // read the local file

        reader.onloadend = function() { // set image data as background of div
            $("#photo1Holder").append($("<img/>", {src:this.result, height:100, width:150}));
            $("#removePhoto1").attr("disabled", false);
        };
        $("#photo1Check").val('changed');
    }
    else {
        return alert(file.name +" nije u JPEG formatu");
    }
});
$(".uploadPhoto2File").on("change", function() {
    var files = !!this.files ? this.files : [];
    if (!files.length || !window.FileReader) return alert("Nije izabrana slika ili nije omogućena podrška"); // no file selected, or no FileReader support

    if (/\.(jpe?g)$/i.test( files[0].name)) { // only jpg file
        $("#photo2Holder").empty();
        var reader = new FileReader(); // instance of the FileReader
        reader.readAsDataURL(files[0]); // read the local file

        reader.onloadend = function() { // set image data as background of div
            $("#photo2Holder").append($("<img/>", {src:this.result, height:100, width:150}));
            $("#removePhoto2").attr("disabled", false);
        };
        $("#photo2Check").val('changed');
    }
    else {
        return alert(file.name +" nije u JPEG formatu");
    }
});$(".uploadPhoto3File").on("change", function() {
    var files = !!this.files ? this.files : [];
    if (!files.length || !window.FileReader) return alert("Nije izabrana slika ili nije omogućena podrška"); // no file selected, or no FileReader support

    if (/\.(jpe?g)$/i.test( files[0].name)) { // only jpg file
        $("#photo3Holder").empty();
        var reader = new FileReader(); // instance of the FileReader
        reader.readAsDataURL(files[0]); // read the local file

        reader.onloadend = function() { // set image data as background of div
            $("#photo3Holder").append($("<img/>", {src:this.result, height:100, width:150}));
            $("#removePhoto3").attr("disabled", false);
        };
        $("#photo3Check").val('changed');
    }
    else {
        return alert(file.name +" nije u JPEG formatu");
    }
});
$(".uploadPhoto4File").on("change", function() {
    var files = !!this.files ? this.files : [];
    if (!files.length || !window.FileReader) return alert("Nije izabrana slika ili nije omogućena podrška"); // no file selected, or no FileReader support

    if (/\.(jpe?g)$/i.test( files[0].name)) { // only jpg file
        $("#photo4Holder").empty();
        var reader = new FileReader(); // instance of the FileReader
        reader.readAsDataURL(files[0]); // read the local file

        reader.onloadend = function() { // set image data as background of div
            $("#photo4Holder").append($("<img/>", {src:this.result, height:100, width:150}));
            $("#removePhoto4").attr("disabled", false);
        };
        $("#photo4Check").val('changed');
    }
    else {
        return alert(file.name +" nije u JPEG formatu");
    }
});
$(".uploadPhoto5File").on("change", function() {
    var files = !!this.files ? this.files : [];
    if (!files.length || !window.FileReader) return alert("Nije izabrana slika ili nije omogućena podrška"); // no file selected, or no FileReader support

    if (/\.(jpe?g)$/i.test( files[0].name)) { // only jpg file
        $("#photo5Holder").empty();
        var reader = new FileReader(); // instance of the FileReader
        reader.readAsDataURL(files[0]); // read the local file

        reader.onloadend = function() { // set image data as background of div
            $("#photo5Holder").append($("<img/>", {src:this.result, height:100, width:150}));
            $("#removePhoto5").attr("disabled", false);
        };
        $("#photo5Check").val('changed');
    }
    else {
        return alert(file.name +" nije u JPEG formatu");
    }
});

$('#removePlan').click(function() {
    $("#planHolder").empty();
    $("#planFeed").val('default.png');
    $("#planHolder").append(
            "<image src='"+ base() + "public/images/apartments/" + $("#planFeed").val()+"' height='100' width='150' />");
    $("#removePlan").attr("disabled", true);
    $("#uploadPlanFile").val(null);
});
$('#removePhoto1').click(function() {
    $("#photo1Holder").empty();
    $("#photo1Feed").val('default.png');
    $("#photo1Holder").append(
            "<image src='"+ base() + "public/images/apartments/" + $("#planFeed").val()+"' height='100' width='150' />");
    $("#removePhoto1").attr("disabled", true);
    $("#uploadPhoto1File").val(null);
});
$('#removePhoto2').click(function() {
    $("#photo2Holder").empty();
    $("#photo2Feed").val('default.png');
    $("#photo2older").append(
            "<image src='"+ base() + "public/images/apartments/" + $("#planFeed").val()+"' height='100' width='150' />");
    $("#removePhoto2").attr("disabled", true);
    $("#uploadPhoto2File").val(null);
});
$('#removePhoto3').click(function() {
    $("#photo3Holder").empty();
    $("#photo3Feed").val('default.png');
    $("#photo3Holder").append(
            "<image src='"+ base() + "public/images/apartments/" + $("#planFeed").val()+"' height='100' width='150' />");
    $("#removePhoto3").attr("disabled", true);
    $("#uploadPhoto3File").val(null);
});
$('#removePhoto4').click(function() {
    $("#photo4Holder").empty();
    $("#photo4Feed").val('default.png');
    $("#photo4Holder").append(
            "<image src='"+ base() + "public/images/apartments/" + $("#planFeed").val()+"' height='100' width='150' />");
    $("#removePhoto4").attr("disabled", true);
    $("#uploadPhoto4File").val(null);
});
$('#removePhoto5').click(function() {
    $("#photo5Holder").empty();
    $("#photo5Feed").val('default.png');
    $("#photo5Holder").append(
            "<image src='"+ base() + "public/images/apartments/" + $("#planFeed").val()+"' height='100' width='150' />");
    $("#removePhoto5").attr("disabled", true);
    $("#uploadPhoto5File").val(null);
});

/* Get base url */
function base()
{
    var urlbase = location.host + '/';

    if((location.href.indexOf("localhost") !== -1) || (location.href.indexOf("http://192.168.") !== -1)){
        var positionLocalHost = location.href.indexOf("localhost") + 9;
        urlbase = location.host + '/'+ location.href.substring(positionLocalHost).split('/')[1] +'/';
    }

    return "http://" + urlbase;
}