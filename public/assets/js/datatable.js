var theDataTable;
var oSettings;  //for datatable settings


//$(document).ready(function() {
theDataTable = $('#datatable').DataTable({

    "sPaginationType": "full_numbers",
    "aaSorting": [],
    "oColVis": {
        "buttonText": "Columns",
        "iOverlayFade": 4,
        "sAlign": "right"
    },
    "oLanguage": {
        "sLengthMenu": '_MENU_ entries per page',
        "sSearch": '<i class="fa fa-search"></i>',
        "oPaginate": {
            "sPrevious": '<i class="fa fa-angle-left"></i>',
            "sNext": '<i class="fa fa-angle-right"></i>'
        }
    }
    });
oSettings = theDataTable.settings();

//});


$("#helpButton").click(function() {
    theDataTable.search('').draw();
    
    //opening all rows before picking up checked values
    oSettings[0]._iDisplayLength= -1;
    theDataTable.draw();

});
