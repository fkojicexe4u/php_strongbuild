/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
    
   
    var eventsArray = [];
    $(".added").each(function () {
        var first = $(this).val().split(';')[0].concat(':\n');
        var second = $(this).val().split(';')[1];

        eventsArray.push({
            title: first.concat(second),
            start: moment($(this).attr('id').split("_").pop()).format("MM-DD-YYYY HH:mm"),
        });
    });

    var calendar = $('#myCalendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,basicWeek,basicDay'
        },
        events: eventsArray
    });


});


$(window).load(function () {
    $(".knob_input").each(function () {
        var max = $(this).attr('data-max');
        var added = $(this).val();

        $(this).val(added.concat('/').concat(max));
    });
});
