var building = 'sve';
var status = 'svi';
var minprice = 0;
var maxprice = 200000;
var minsize = 0;
var maxsize = 10000;
$('.filter').on("change", function(){
    if ($(this).attr('id') == "BuildingSelect"){
        if ($(this).val() != null){
            building = $(this).val();
        }
        else{
            building = 'sve';
        }
    }
    else if ($(this).attr('id') == 'StatusSelect'){
        if ($(this).val() != null){
            status = $(this).val();
        }
        else{
            status = 'sve';
        }
    }
    else if ($(this).attr('id') == 'SizeFrom'){
        if ($(this).val() != null){
            minsize = $(this).val();
        }
        else{
            minsize = 0;
        }
    }
    else if ($(this).attr('id') == 'SizeTo'){
        if ($(this).val() != null){
            maxsize = $(this).val();
        }
        else{
            maxsize = 10000;
        }
    }
    else if ($(this).attr('id') == 'PriceFrom'){
        if ($(this).val() != null){
            minprice = $(this).val();
        }
        else{
            minprice = 0;
        }
    }
    else if ($(this).attr('id') == 'PriceTo'){
        if ($(this).val() != null){
            maxprice = $(this).val();
        }
        else{
            maxprice = 200000;
        }
    }
    hideApartments(building, status, minsize, maxsize, minprice, maxprice);
});

function hideApartments(building, status, minsize, maxsize, minprice, maxprice){
    $('.property-box-2').each(function(){
        $(this).addClass('hidden');
    });
    $('.property-box-2').each(function(){
        var apartsBuilding = $(this).find('#building_name').attr('value');
        var apartsStatus = $(this).find('#status').attr('value');
        var apartsSize = $(this).find('#size').attr('value');
        var apartsPrice = $(this).find('#price').attr('value');
        var checker = false;
        console.log(apartsBuilding);
        if (building == 'sve' || building == apartsBuilding){
            checker = true;
        }
        if (checker){
            if (status == 'svi' || status == apartsStatus){
                checker = true;
            }
            else{
                checker = false;
            }
        }
        if (checker){
            if (apartsSize > minsize && apartsSize < maxsize && apartsPrice > minprice && apartsPrice < maxprice){
                checker = true;
            }
            else{
                checker = false;
            }
        }
        if (checker){
            $(this).removeClass('hidden');
        }
    });
}
