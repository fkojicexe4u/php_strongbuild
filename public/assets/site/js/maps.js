function initMap() {
  // Belgrade coordinates
    var coord = {lat: 44.7991212, lng: 20.4856257};
  // The map, centered at Belgrade
    var map = new google.maps.Map(
      document.getElementById('map'), {zoom: 13, center: coord});
  // The markers
    var adresses = [];
    $('.property-box-2').each(function(i){
        adresses[i] = $(this).find('.location').val();
    });
    for (var x = 0; x < adresses.length; x++) {
        $.getJSON('https://maps.googleapis.com/maps/api/place/findplacefromtext/json?'+adresses[x], null, function (data) {
            var p = data.results[0].geometry.location;
            var latlng = new google.maps.LatLng(p.lat, p.lng);
            new google.maps.Marker({
                position: latlng,
                map: map
            });
        });
    }
  //var marker = new google.maps.Marker({position: uluru, map: map});
}