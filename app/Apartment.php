<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;

use Illuminate\Support\Facades\DB;

class Apartment extends Model
{
    use AuthenticableTrait;
    
    protected $table = 'apartments';
    
    protected $fillable = ['building_id', 'bay', 'floor', 'number', 'structure', 'size', 'unit_price', 'showing_full_price', 'status_id'];
    
    protected $hidden = ['id'];
    
    public $timestamps = false; //model(and database table) should not have time stamps
}
