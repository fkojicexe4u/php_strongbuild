<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;

use Illuminate\Support\Facades\DB;

class Status extends Model
{
    use AuthenticableTrait;
    
    protected $table = 'status';
    
    protected $fillable = ['name'];
    
    protected $hidden = ['id'];
    
    public $timestamps = false; //model(and database table) should not have time stamps
}