<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/', 'HomeController@index');
Route::get('flats', 'Homecontroller@flats');

Route::get('admin/', function () {
    return view('admin.auth.login');
});

Route::auth();

Route::group(array('prefix' => 'admin', 'middleware' => 'auth'), function() {

    Route::get('home', 'Admin\AdminController@index');

    /* 
    |---------------------------------------------------------------------------
    | Buildings: all, edit, add, delete, photos (with add, change and delete)
    |---------------------------------------------------------------------------
    */
    Route::resource('buildings', 'Admin\BuildingsController');
    Route::delete('buildings/delete/{id}', 'Admin\BuildingsController@delete');
    Route::get('buildings/photos/{id}','Admin\BuildingsController@photos');
    Route::get('buildings/addPhoto/{id}', 'Admin\BuildingsController@addPhoto');
    Route::post('buildings/storePhoto', 'Admin\BuildingsController@storePhoto');
    Route::get('buildings/changePhoto/{id}', 'Admin\BuildingsController@changePhoto');
    Route::post('buildings/updatePhoto', 'Admin\BuildingsController@updatePhoto');
    Route::get('buildings/deletePhoto/{id}', 'Admin\BuildingsController@deletePhoto');
    /* 
    |---------------------------------------------------------------------------
    |Apartments: all, edit, add, delete, clone, photos
    |---------------------------------------------------------------------------
    */
    Route::resource('apartments', 'Admin\ApartmentsController');
    Route::delete('apartments/delete/{id}', 'Admin\ApartmentsController@delete');
    //===Copy and store apartment===
    Route::get('apartments/copy/{id}', 'Admin\ApartmentsController@copy');
    Route::post('apartments/storeCopy', 'Admin\ApartmentsController@storeCopy');
    //===Show photos of apartment===
    Route::get('apartments/photos/{id}', 'Admin\ApartmentsController@photos');
    //===Changing status of aparrtment with button in apartments table===
    Route::get('apartments/status/{id}', 'Admin\ApartmentsController@status');
});