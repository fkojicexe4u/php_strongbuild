<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Apartment;
use App\Building;
use App\Status;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\File;

class ApartmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $apartments = Apartment::all();
        foreach ($apartments as $apartment){
            $apartment->building_id = Building::findOrFail($apartment->building_id)->name;
            $apartment->status_id = Status::findOrFail($apartment->status_id)->name;
        }

        return view('admin/apartments/allapartments', ['active' => 'allApartments', 'apartments' => $apartments]);
    }
    public function create ()
    {
        $buildings = Building::all();
        $statuses = Status::all();
        return view('admin/apartments/create', ['active' => 'addApartment', 'buildings' => $buildings, 'statuses' => $statuses]);
    }
    public function store(Request $request)
    {        
        $apartment = new Apartment;
        $apartment->building_id = $request->building_id;
        $apartment->bay = $request->bay;
        $apartment->floor = $request->floor;
        $apartment->number = $request->number;
        $apartment->structure = $request->structure;
        $apartment->size = $request->size;
        $apartment->unit_price = $request->unit_price;
        $apartment->showing_full_price = $request->showing_full_price;
        $apartment->status_id = $request->status_id;
        $apartment->save();
        $id = Apartment::orderBy('id', 'desc')->first()->id;

        if ($request->file('plan')->isValid()) {
            $apartment->plan = $id.'_plan.jpg';
            Image::make($request->file('plan')->getRealPath())->resize(800, 600)->save(public_path('images/apartments/').$apartment->plan);
        }
        if ($request->file('image_one')->isValid()) {
            $apartment->image_one = $id.'_slika1.jpg';
            Image::make($request->file('image_one')->getRealPath())->resize(800, 600)->save(public_path('images/apartments/').$apartment->image_one);
        }
        if ($request->photo2_check == 'changed') {
            if ($request->file('image_two')->isValid()){
                $apartment->image_two = $id.'_slika2.jpg';
                Image::make($request->file('image_two')->getRealPath())->resize(800, 600)->save(public_path('images/apartments/').$apartment->image_two);
            }
        }
        else{
            $apartment->image_two = 'default.png';
        }
        if ($request->photo3_check == 'changed'){
            if ($request->file('image_three')->isValid()){
                $apartment->image_three = $id.'_slika3.jpg';
                Image::make($request->file('image_three')->getRealPath())->resize(800, 600)->save(public_path('images/apartments/').$apartment->image_three);
            }
        }
        else{
            $apartment->image_three = 'default.png';
        }
        if ($request->photo4_check == 'changed'){
            if ($request->file('image_four')->isValid()){
                $apartment->image_four = $id.'_slika4.jpg';
                Image::make($request->file('image_four')->getRealPath())->resize(800, 600)->save(public_path('images/apartments/').$apartment->image_four);
            }
        }
        else{
            $apartment->image_four = 'default.png';
        }
        if ($request->photo5_check == 'changed'){
            if ($request->file('image_five')->isValid()){
                $apartment->image_five = $id.'_slika5.jpg';
                Image::make($request->file('image_five')->getRealPath())->resize(800, 600)->save(public_path('images/apartments/').$apartment->image_five);
            }
        }
        else{
            $apartment->image_five = 'default.png';
        }
        $apartment->save();
        
        Session::flash('message', 'success_Stan je dodat!');
        
        return redirect('admin/apartments');
    }
    public function edit($id)
    {
        $buildings = Building::all();
        $statuses = Status::all();
        $apartment = Apartment::findOrFail($id);
        
        return view ('admin/apartments/edit', ['active' => 'addApartment', 'apartment' => $apartment, 'buildings' => $buildings, 'statuses' => $statuses]);
    }
    public function update(Request $request, $id)
    {
        $apartment = Apartment::findOrFail($id);
    $apartment->update([$request->building_id, $request->bay, $request->floor, $request->number, $request->structure, $request->size, $request->unit_price, $request->showing_full_price, $request->status_id]);
        if ($request->plan_check == 'changed'){
            unlink(public_path('images/apartments/'.$id.'_plan.jpg'));
            if ($request->file('plan')->isValid()) {
                Image::make($request
                    ->file('plan')
                    ->getRealPath())->resize(800, 600)
                        ->save(public_path('images/apartments/'.$id.'_plan.jpg'));
            }
        }
        if ($request->photo1_check == 'changed'){
            unlink(public_path('images/apartments/'.$id.'_slika1.jpg'));
            if ($request->file('image_one')->isValid()) {
                Image::make($request
                    ->file('image_one')
                    ->getRealPath())->resize(800, 600)
                        ->save(public_path('images/apartments/'.$id.'_slika1.jpg'));
            }
        }
        if ($request->photo2_check == 'changed'){
            if ($apartment->image_two != 'default.png'){
                unlink(public_path('images/apartments/'.$id.'_slika2.jpg'));
            }
            if ($request->file('image_two')->isValid()) {
                Image::make($request
                    ->file('image_two')
                    ->getRealPath())->resize(800, 600)
                        ->save(public_path('images/apartments/'.$id.'_slika2.jpg'));
            }
            $apartment->image_two = $id.'_slika2.jpg';
        }
        if ($request->photo3_check == 'changed'){
            if ($apartment->image_three != 'default.png'){
                unlink(public_path('images/apartments/'.$id.'_slika3.jpg'));
            }
            if ($request->file('image_three')->isValid()) {
                Image::make($request
                    ->file('image_three')
                    ->getRealPath())->resize(800, 600)
                        ->save(public_path('images/apartments/'.$id.'_slika3.jpg'));
            }
            $apartment->image_three = $id.'_slika3.jpg';
        }
        if ($request->photo4_check == 'changed'){
            if ($apartment->image_four != 'default.png'){
                unlink(public_path('images/apartments/'.$id.'_slika4.jpg'));
            }
            if ($request->file('image_four')->isValid()) {
                Image::make($request
                    ->file('image_four')
                    ->getRealPath())->resize(800, 600)
                        ->save(public_path('images/apartments/'.$id.'_slika4.jpg'));
            }
            $apartment->image_four = $id.'_slika4.jpg';
        }
        if ($request->photo5_check == 'changed'){
            if ($apartment->image_five != 'default.png'){
                unlink(public_path('images/apartments/'.$id.'_slika5.jpg'));
            }
            if ($request->file('image_five')->isValid()) {
                Image::make($request
                    ->file('image_five')
                    ->getRealPath())->resize(800, 600)
                        ->save(public_path('images/apartments/'.$id.'_slika5.jpg'));
            }
            $apartment->image_five = $id.'_slika5.jpg';
        }
        $apartment->save();

        Session::flash('message', 'success_Stan je uređen!');
        
        return redirect('admin/apartments');
        
    }
    public function delete($id)
    {
        $apartment = Apartment::findOrFail($id);
        unlink(public_path('images/apartments/'.$apartment->plan));
        unlink(public_path('images/apartments/'.$apartment->image_one));
        if($apartment->image_two != 'default.png'){
            unlink(public_path('images/apartments/'.$apartment->image_two));
        }
        if($apartment->image_three != 'default.png'){
            unlink(public_path('images/apartments/'.$apartment->image_three));
        }
        if($apartment->image_four != 'default.png'){
            unlink(public_path('images/apartments/'.$apartment->image_four));
        }
        if($apartment->image_five != 'default.png'){
            unlink(public_path('images/apartments/'.$apartment->image_five));
        }
        $apartment->delete();
        
        Session::flash('message', 'danger_Stan je obrisan!');
        
        return redirect('admin/apartments');
    }
    public function copy($id)
    {
        $buildings = Building::all();
        $statuses = Status::all();
        $apartment = Apartment::findOrFail($id);
        
        return view ('admin/apartments/copy', ['active' => 'addApartment', 'apartment' => $apartment, 'buildings' => $buildings, 'statuses' => $statuses]);
    }
    public function storeCopy(Request $request)
    {

        $old_apartment = Apartment::findOrFail($request->apartment_id);
        $apartment = new Apartment;
        $apartment->building_id = $request->building_id;
        $apartment->bay = $request->bay;
        $apartment->floor = $request->floor;
        $apartment->number = $request->number;
        $apartment->structure = $request->structure;
        $apartment->size = $request->size;
        $apartment->unit_price = $request->unit_price;
        $apartment->showing_full_price = $request->showing_full_price;
        $apartment->status_id = $request->status_id;
        $apartment->save();
        $id = Apartment::orderBy('id', 'desc')->first()->id;
        $apartment->plan = $id.'_plan.jpg';
        $apartment->image_one = $id.'_slika1.jpg';
        $apartment->image_two = $id.'_slika2.jpg';
        $apartment->image_three = $id.'_slika3.jpg';
        $apartment->image_four = $id.'_slika4.jpg';
        $apartment->image_five = $id.'_slika5.jpg';
        if($request->plan_check != 'changed'){
            File::copy(asset('images/apartments/'.$old_apartment->plan), public_path('images/apartments/'.$apartment->plan));
        }
        else{
           if ($request->file('plan')->isValid()) {
               Image::make($request->file('plan')->getRealPath())->resize(800, 600)->save(public_path('images/apartments/').$apartment->plan);
           } 
        }
        if($request->photo1_check != 'changed'){
            File::copy(asset('images/apartments/'.$old_apartment->image_one), public_path('images/apartments/'.$apartment->image_one));
        }
        else{
           if($request->file('image_one')->isValid()) {
               Image::make($request->file('image_one')->getRealPath())->resize(800, 600)->save(public_path('images/apartments/').$apartment->image_one);
           } 
        }
        if($request->photo2_check != 'changed'){
            if($old_apartment->image_two != 'default.png'){
                File::copy(asset('images/apartments/'.$old_apartment->image_two), public_path('images/apartments/'.$apartment->image_two));
            }
            else{
                $apartment->image_two = 'default.png';
            }
        }
        else{
            if($request->file('image_two')->isValid()) {
               Image::make($request->file('image_two')->getRealPath())->resize(800, 600)->save(public_path('images/apartments/').$apartment->image_two);
           } 
        }
        if($request->photo3_check != 'changed'){
            if($old_apartment->image_three != 'default.png'){
                File::copy(asset('images/apartments/'.$old_apartment->image_three), public_path('images/apartments/'.$apartment->image_three));
            }
            else{
                $apartment->image_three = 'default.png';
            }
        }
        else{
            if($request->file('image_three')->isValid()) {
               Image::make($request->file('image_three')->getRealPath())->resize(800, 600)->save(public_path('images/apartments/').$apartment->image_three);
           } 
        }
        if($request->photo4_check != 'changed'){
            if($old_apartment->image_four != 'default.png'){
                File::copy(asset('images/apartments/'.$old_apartment->image_four), public_path('images/apartments/'.$apartment->image_four));
            }
            else{
                $apartment->image_four = 'default.png';
            }
        }
        else{
            if($request->file('image_four')->isValid()) {
               Image::make($request->file('image_four')->getRealPath())->resize(800, 600)->save(public_path('images/apartments/').$apartment->image_four);
           } 
        }
        if($request->photo5_check != 'changed'){
            if($old_apartment->image_five != 'default.png'){
                File::copy(asset('images/apartments/'.$old_apartment->image_five), public_path('images/apartments/'.$apartment->image_five));
            }
            else{
                $apartment->image_five = 'default.png';
            }
        }
        else{
            if($request->file('image_five')->isValid()) {
               Image::make($request->file('image_five')->getRealPath())->resize(800, 600)->save(public_path('images/apartments/').$apartment->image_five);
           } 
        }
        $apartment->save();
        
        Session::flash('message', 'success_Stan je kloniran!');
        
        return redirect('admin/apartments');
    }
    public function status($id)
    {
        list ($apartment_id, $status) = explode('_', $id);
        $apartment = Apartment::findOrFail($apartment_id);
        $apartment->status_id = $status;
        $apartment->save();
        
        Session::flash('message', 'success_Status je promenjen!');
        
        return redirect('admin/apartments');
    }
    public function photos($id)
    {
        $apartment = Apartment::findOrFail($id);
        return view ('admin/apartments/photos', ['active' => 'allApartments', 'apartment' => $apartment]);
    }
}