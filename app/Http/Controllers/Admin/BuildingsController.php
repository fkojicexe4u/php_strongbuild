<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Building;
use App\Apartment;
use App\BuildingPhotos;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Intervention\Image\ImageManagerStatic as Image;


class BuildingsController extends Controller
{
  
    public function index()
    {
        $buildings = Building::all();

        return view('/admin/buildings/allbuildings', ['active' => 'allBuldings', 'buildings' => $buildings]);
    }
    public function create ()
    {
        return view('/admin/buildings/create', ['active' => 'addBuilding']);
    }
    public function store(Request $request)
    {    
        $building = Building::create($request->except('main_image', 'plans', 'photos'));   
        $building->save();
        
        $id = Building::orderBy('id', 'desc')->first()->id;
        if ($request->file('main_image')->isValid()) {
            Image::make($request
                    ->file('main_image')
                    ->getRealPath())->resize(800, 600)
                        ->save(public_path('images/buildings/').strval($id).'_naslovna.jpg');
        }
        $plan_id = 0;
        $photo_id = 0;
        $key = 0;
        $db_input_data = [];
        foreach ($request->file('plans') as $plan){
            $plan_id ++;
            $db_input_data[$key]['building_id'] = $id;
            $db_input_data[$key]['photo_type'] = 'plan';
            $db_input_data[$key]['photo_id'] = $plan_id;
            $key++;
            Image::make($plan
                    ->getRealPath())->resize(800, 600)
                        ->save(public_path('images/buildings/').strval($id).'_plan_'.$plan_id.'.jpg');
        }
        foreach ($request->file('photos') as $photo){
            $photo_id ++;
            $db_input_data[$key]['building_id'] = $id;
            $db_input_data[$key]['photo_type'] = 'slika';
            $db_input_data[$key]['photo_id'] = $photo_id;
            $key++;
            Image::make($photo
                    ->getRealPath())->resize(800, 600)
                        ->save(public_path('images/buildings/').strval($id).'_slika_'.$photo_id.'.jpg');
        }
        BuildingPhotos::insertArray($db_input_data);
        
        Session::flash('message', 'success_Zgrada je dodata!');
        
        return redirect('admin/buildings');
    }
    public function edit($id)
    {
        $building = Building::findOrFail($id);
        $main_image  = strval($id).'_naslovna.jpg';
        $plans_feed = BuildingPhotos::getFeedString($id, 'plan');
        $photos_feed = BuildingPhotos::getFeedString($id, 'slika');
        return view ('admin/buildings/edit', ['active' => 'addBuilding', 'building' => $building, 'main_image' => $main_image, 'plans_feed' => $plans_feed, 'photos_feed' => $photos_feed]);
    }
    public function update(Request $request)
    {   
        $id = $request->building_id;
        $building = Building::findOrFail($id);
        
        if ($request->main_image_check == 'changed'){
            unlink(public_path('images/buildings/'.strval($id).'_naslovna.jpg'));
            if ($request->file('main_image')->isValid()) {
                Image::make($request
                    ->file('main_image')
                    ->getRealPath())->resize(800, 600)
                        ->save(public_path('images/buildings/').strval($id).'_naslovna.jpg');
            }
        }
        if ($request->plans_check == 'changed'){
            $this->changeImages($request, 'plan');
        }
        if ($request->photos_check == 'changed'){
            $this->changeImages($request, 'slika');
        }
        
        $building->update($request->except('main_image', 'plans', 'photos'));
        $building->save();

        Session::flash('message', 'success_Zgrada je uređena!');
        
        return redirect('admin/buildings');
        
    }
    public function delete($id)
    {
        $building = Building::findOrFail($id);
        $photos = BuildingPhotos::where('building_id', '=', $id)->get();
        if(!$photos->isEmpty()){
            foreach($photos as $photo){
                unlink(public_path('images/buildings/'.$id.'_'.$photo->photo_type.'_'.$photo->photo_id.'.jpg'));
                $photo->delete();
            }
        }
        unlink(public_path('images/buildings/'.strval($id).'_naslovna.jpg'));
        $building->deleteBelongingApartments();
        $building->delete();
        
        Session::flash('message', 'danger_Zgrada je obrisana!');
        
        return redirect('admin/buildings');
    }
    public function photos($id)
    {
        $photos = BuildingPhotos::getPhotos($id);
        $plans = BuildingPhotos::getPlans($id);
        $building = Building::findOrFail($id);
        $main_image_path = asset('/images/buildings/'.strval($id).'_naslovna.jpg');
        $plan_path = [];
        $photo_path = [];
        foreach ($plans as $plan){
            $plan_path[$plan->id] = asset('/images/buildings/'.strval($id).'_plan_'.strval($plan->photo_id).'.jpg');
        }
        foreach ($photos as $photo){
            $photo_path[$photo->id] = asset('/images/buildings/'.strval($id).'_slika_'.strval($photo->photo_id).'.jpg');
        }
        
        return view('admin/buildings/allbuildingphotos', ['active' => 'addBuilding', 'plans' => $plans, 'photos' => $photos, 'building' => $building,
            'main_image_path' => $main_image_path, 'plan_path' => $plan_path, 'photo_path' => $photo_path]);
    }
    public function addPhoto($id)
    {
        list ($photo_type, $building_id) = explode('_', $id);
        $feed = 'default.png';
        
        return view('admin/buildings/addphoto', ['active' => 'addBuilding', 'id' => $id, 'building_id' => $building_id, 'photo_type' => $photo_type, 'feed' => $feed]);
    }
    public function storePhoto(Request $request)
    {
        if ($request->file('main_image')->isValid()) {
            $photo = new BuildingPhotos;
            $photo->building_id = $request->building_id;
            $photo->photo_type = $request->photo_type;
            $last_photo_id = BuildingPhotos::where('building_id', '=', $photo->building_id)
                    ->where('photo_type', '=', $photo->photo_type)
                    ->orderBy('id', 'desc')
                    ->first()->photo_id;
            if ($last_photo_id){
                $photo->photo_id = $last_photo_id + 1;
            }
            else{
                $photo->photo_id = 1;
            }
            $photo->save();
            Image::make($request->file('main_image')->getRealPath())
                    ->resize(800, 600)
                    ->save(public_path('images/buildings/').$photo->building_id.'_'.$photo->photo_type.'_'.$photo->photo_id.'.jpg');
        }
        Session::flash('message', 'success_Slika je dodata!');
        
        return redirect('admin/buildings/photos/'.$photo->building_id);
    }
    public function changePhoto($id)
    {
        $check_type = 'naslovna';
        if (substr($id, 0, strlen($check_type)) == $check_type){
            list ($photo_type, $building_id) = explode('_', $id);
            $feed = $building_id.'_naslovna.jpg';
        }
        else{
            $photo = BuildingPhotos::findOrFail($id);
            $photo_type = $photo->photo_type;
            $building_id = $photo->building_id;
            $id = $photo->photo_id;
            $feed = $building_id.'_'.$photo_type.'_'.$id.'.jpg';
        }
        
        return view('admin/buildings/changephoto', ['active' => 'addBuilding', 'id' => $id, 'building_id' => $building_id, 'photo_type' => $photo_type, 'feed' => $feed]);
    }
    public function updatePhoto(Request $request)
    {
        $this->validate($request, [
            'main_image' => 'required|image|mimes:jpeg,jpg|max:2048' //maksimalna velicina slike 2MB (2048 kB)
        ]);
        if ($request->photo_type == 'naslovna'){
            $photo_name = $request->building_id.'_'.$request->photo_type.'.jpg';
        }
        else{
            $photo_name = $request->building_id.'_'.$request->photo_type.'_'.$request->id.'.jpg';
        }
        unlink(public_path('images/buildings/'.$photo_name));
        Image::make($request->file('main_image')->getRealPath())
                ->resize(800, 600)
                ->save(public_path('images/buildings/').$photo_name);
        Session::flash('message', 'success_Slika je promenjena!');
        
        return redirect('admin/buildings/photos/'.$request->building_id);
    }
    public function deletePhoto($id)
    {
        $photo = BuildingPhotos::findOrFail($id);
        $building_id = $photo->building_id;
        $path = public_path('images/buildings/'.$building_id.'_'.$photo->photo_type.'_'.$photo->photo_id.'.jpg');
        $photo->delete();
        unlink($path);
        
        Session::flash('message', 'danger_Slika je obrisana!');
        
        return redirect('admin/buildings/photos/'.$building_id);
    }
    private function changeImages(Request $request, $photo_type)
    {
        $photos = BuildingPhotos::where('building_id', '=', $request->building_id)->where('photo_type', '=', $photo_type)->get();
        if(!$photos->isEmpty()){
            foreach($photos as $photo){
                unlink(public_path('images/buildings/'.$photo->building_id.'_'.$photo->photo_type.'_'.$photo->photo_id.'.jpg'));
                $photo->delete();
            }
        }
        $last_image_id = BuildingPhotos::where('building_id', '=', $request->building_id)
                    ->where('photo_type', '=', $photo_type)
                    ->orderBy('id', 'desc')
                    ->first();
        if($last_image_id){
            $image_id = $last_image_id->photo_id + 1;
        }
        else{
            $image_id = 1;
        }
        $new_photos = [];
        if ($photo_type == 'plan'){ $holder = 'plans';}
        if ($photo_type == 'slika'){ $holder = 'photos';}
        $key = 0;
        foreach ($request->file($holder) as $image){
            $new_photos[$key]['building_id'] = $request->building_id;
            $new_photos[$key]['photo_type'] = $photo_type;
            $new_photos[$key]['photo_id'] = $image_id;
            Image::make($image
                    ->getRealPath())->resize(800, 600)
                        ->save(public_path('images/buildings/').strval($request->building_id).'_'.$photo_type.'_'.$image_id.'.jpg');
            $image_id ++;
            $key++;
        }
        BuildingPhotos::insertArray($new_photos);
    }
}