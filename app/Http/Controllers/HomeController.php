<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Apartment;
use App\Building;
use App\Status;
use App\BuildingPhotos;

class HomeController extends Controller
{


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $buildings = Building::all();
        $num_of_apartments = [];
        foreach ($buildings as $building){
            $num_of_apartments[$building->id] = Apartment::where('building_id', '=', $building->id)->count();
        }
        
        return view('home', ['active' => 'pocetna', 'buildings' => $buildings, 'num_of_apartments' => $num_of_apartments, 'page_title' => 'Dobrodošli']);
    }
    
    public function flats()
    {
        $apartments = Apartment::all();
        $building_list = Building::all();
        $statuses = Status::all();
        $buildings = [];
        $price = [];
        foreach ($apartments as $apartment){
            foreach($building_list as $building){
                if ($apartment->building_id == $building->id){
                    $buildings[$apartment->id] = $building;
                }
            }
            foreach($statuses as $status){
                if($apartment->status_id == $status->id){
                    $apartment->status_id = $status->name;
                }
            }
            if ($apartment->showing_full_price){
                $price[$apartment->id] = $apartment->size * $apartment->unit_price;
            }
        }
        
        return view('apartments', ['active' => 'Stanovi', 'page_title' => 'Lista svih stanova', 'apartments' => $apartments, 'buildings' => $buildings, 'building_list' => $building_list, 'statuses' => $statuses, 'price' => $price]);
    }
}
