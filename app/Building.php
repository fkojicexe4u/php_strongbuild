<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;
use App\Apartment;

use Illuminate\Support\Facades\DB;

class Building extends Model
{
    use AuthenticableTrait;
    
    protected $table = 'buildings';
    
    protected $fillable = ['name', 'adress', 'municipality', 'city', 'built_date', 'bays_number', 'floors_number'];
    
    protected $hidden = ['id'];
    
    public $timestamps = false; //model(and database table) should not have time stamps
    
    public function deleteBelongingApartments()
    {
        $apartments = Apartment::where('building_id', '=', $this->id)->get();
        if (!$apartments->isEmpty()){
            foreach($apartments as $apartment){
                unlink(public_path('images/apartments/'.$apartment->plan));
                unlink(public_path('images/apartments/'.$apartment->image_one));
                if ($apartment->image_two != 'default.png'){
                    unlink(public_path('images/apartments/'.$apartment->image_two));
                }
                if ($apartment->image_three != 'default.png'){
                    unlink(public_path('images/apartments/'.$apartment->image_three));
                }
                if ($apartment->image_four != 'default.png'){
                    unlink(public_path('images/apartments/'.$apartment->image_four));
                }
                if ($apartment->image_five != 'default.png'){
                    unlink(public_path('images/apartments/'.$apartment->image_five));
                }
                $apartment->delete(); 
            }
        }
    }
}
