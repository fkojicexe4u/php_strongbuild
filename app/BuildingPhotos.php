<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;

use Illuminate\Support\Facades\DB;

class BuildingPhotos extends Model
{
    use AuthenticableTrait;
    
    protected $table = 'building_photos';
    
    protected $fillable = ['building_id', 'photo_type', 'photo_id'];
    
    protected $hidden = ['id'];
    
    public $timestamps = false; //model(and database table) should not have time stamps
    
    public static function getPhotos($building_id)
    {
        return DB::table('building_photos')->where('building_id', '=', $building_id)->where('photo_type', '=', 'slika')->get();
    }
    
    public static function getPlans($building_id)
    {
        return DB::table('building_photos')->where('building_id', '=', $building_id)->where('photo_type', '=', 'plan')->get();
    }
    public static function insertArray ($array)
    {
        foreach ($array as $row){
            DB::table('building_photos')->insert($row);
        }
    }
    public static function getFeedString ($building_id, $photo_type)
    {
        $images = DB::table('building_photos')->where('building_id', '=', $building_id)->where('photo_type', '=', $photo_type)->get();
        $string = '';
        foreach($images as $image){
            $string = $string.$image->building_id.'_'.$image->photo_type.'_'.$image->photo_id.'.jpg|';
        }
        if (substr($string, -1) == "|"){
            $string = substr_replace($string ,"", -1);
        }
        return $string;
    }
}
