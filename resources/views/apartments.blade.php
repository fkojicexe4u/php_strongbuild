@extends('layouts.layout')

@section('content')

<!-- Apartments section body start -->
<div class="property-content content-area container-fluid">
    <div class="row">
        <div class="col-lg-6 content-area-6">
            <!-- Property box 2 start -->
            @foreach($apartments as $apartment)
            <div class="property-box-2" >
                <div class="row">
                    <div class="col-lg-4 col-md-5 col-pad">
                        <div class="property-thumbnail">
                            <a href="{{URL('/apartment/'.$apartment->id)}}" class="property-img">
                                <img src="{{asset('/images/apartments/'.$apartment->plan)}}" alt="big-properties" class="img-fluid">
                                <div class="listing-badges">
                                    <span class="featured {{ $apartment->status_id }}">{{ $apartment->status_id }}</span>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-7 col-pad">
                        <div class="detail">
                            <div class="hdg">
                                <h3 class="title">
                                    <a href="{{URL('/apartment/'.$apartment->id)}}">Stan broj {{$apartment->number}}</a>
                                </h3>
                                <h5 class="location">
                                    <a href="{{URL('/building/'.$apartment->building_id)}}">
                                        <i class="flaticon-pin"></i>{{$buildings[$apartment->id]->name}}: {{$buildings[$apartment->id]->adress}}
                                    </a>
                                </h5>
                            </div>
                            <ul class="facilities-list clearfix">
                                <li>
                                    <span>Struktura</span> {{$apartment->structure}}
                                </li>
                                <li>
                                    <span>Površina</span> {{$apartment->size}}
                                </li>
                                <li>
                                    <span>Lamela</span> {{$apartment->bay}}
                                </li>
                                <li>
                                    <span>Sprat</span> {{$apartment->floor}}
                                </li>
                                <li>
                                    <span>Cena kvadrata</span> {{$apartment->unit_price}} <i class="fa fa-euro"></i>
                                </li>
                            </ul>
                            <div class="footer">
                                @if ($apartment->showing_full_price)
                                    {{$price[$apartment->id]}}<i class="fa fa-euro"></i>
                                @endif
                                <span>
                                      <i class="fa fa-calendar-check-o"></i>{{$buildings[$apartment->id]->built_date}}
                                </span>
                            </div>
                            <span class='hidden' id='building_name' style='display:none' value='{{$buildings[$apartment->id]->name}}'/>
                            <span class="hidden" id="status" style="display:none" value="{{$apartment->status_id}}">
                            <span class='hidden' id='structure' style='display:none' value='{{$apartment->structure}}'/>
                            <span class='hidden' id='size' style='display:none' value='{{$apartment->size}}'/>
                            <span class='hidden' id='price' style='display:none' value='{{$apartment->unit_price}}'/>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <div class="col-lg-6 widget-2">
            <div class="sidebar-title ">
                <h3>Filtriranje liste</h3>
            </div>
            <div class='sidebar-content'>
                <form method="GET">
                    <div class="form-label">
                        <label>Zgrada:</label>
                    </div>
                    <div class="form-group">
                        <select class="search-fields col-lg-6 filter" id="BuildingSelect" name="building">
                            <option value="sve">Sve zgrade</option>
                            @foreach($building_list as $single_building)
                            <option>{{$single_building->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-label">
                        <label>Status:</label>
                    </div>
                    <div class="form-group">
                        <select class="search-fields col-lg-6 filter" id="StatusSelect" name="status">
                            <option value="svi">Svi statusi</option>
                            @foreach($statuses as $status)
                            <option>{{$status->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-label">
                        <label>Kvadratura:</label>
                    </div>
                    <div class="form-group">
                        <label class="inline col-md-1">od: </label>
                        <input class="search-fields col-lg-2 filter inline-search-small" id="SizeFrom" default-value="0" name="size_from"/>
                        <label class="inline col-md-1">do: </label>
                        <input class="search-fields col-lg-2 filter inline-search-small" id="SizeTo" name="size_to"/>
                    </div>
                    <div class="form-label">
                        <label>Cena kvadrata:</label>
                    </div>
                    <div class="form-group">
                        <label class="inline col-md-1">od: </label>
                        <input class="search-fields col-lg-2 filter inline-search-small" id="PriceFrom" default-value="0" name="price_from"/>
                        <label class="inline col-md-1">do: </label>
                        <input class="search-fields col-lg-2 filter inline-search-small" id="PriceTo" name="price_to"/>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Apartments section body end -->
@stop
@section('pageScripts')
<script src="{{ asset('assets/site/js/filter.js') }}"></script>
@stop