@extends('layouts.layout')

@section('content')

<div class="map-content content-area container-fluid">
    <div class="row">
        <div class="col-lg-7 map-content-sidebar">
            <div class="properties-map-search properties-pad2">
                <div class="fetching-properties pad-0-15 hidden-sm hidden-xs">
                    @foreach ($buildings as $building)
                    <div class="property-box-2" id="{{$building->id}}">
                        <div class="row">
                            <div class="col-lg-5 col-md-5 col-pad">
                                <div class="property-thumbnail">
                                    <a href="{{URL('/buildings/'.$building->id)}}" class="property-img">
                                        <img src="{{asset('/images/buildings/'.$building->id.'_naslovna.jpg')}}" alt="properties" class="img-fluid">
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-7 col-md-7 col-pad">
                                <div class="detail">
                                    <div class="hdg">
                                        <h3 class="title">
                                            <a href="{{URL('/buildings/'.$building->id)}}">{{$building->name}}</a>
                                        </h3>
                                        <h5 class="location">
                                            <a href="{{URL('/buildings/'.$building->id)}}">
                                                <i class="flaticon-location"></i>{{$building->adress}}, {{$building->municipality}}, {{$building->city}}
                                            </a>
                                        </h5>
                                    </div>
                                    <ul class="facilities-list clearfix">
                                        <li>
                                            <span>Lamela:</span> {{$building->bays_number}}
                                        </li>
                                        <li>
                                            <span>Spratova:</span> {{$building->floors_number}}
                                        </li>
                                        <li>
                                            <span>Stanova:</span> {{$num_of_apartments[$building->id]}}
                                        </li>
                                    </ul>
                                    <div class="footer">
                                        <span>
                                              <i class="flaticon-calendar"></i>{{$building->built_date}}
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="col-lg-5">
            <div class="row">
                <div id="map"></div>
            </div>
        </div>
    </div>
</div>

@stop
@section('pageScripts')
<script src="{{ asset('assets/site/js/maps.js') }}"></script>
<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCI9iyYtMfgLBMeVe_lW_HqNJwuzTISOYo&callback=initMap" >
</script>

@stop