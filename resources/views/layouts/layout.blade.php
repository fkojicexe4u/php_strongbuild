<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Google Tag Manager -->

        <!-- End Google Tag Manager -->
        <title>Strongbuild</title>
        @include('layouts.head') 
    </head>
    <body >
        <!-- BEGIN HEADER-->
        @include('layouts.header')
        <!-- END HEADER-->
        <!-- BEGIN CONTENT-->
        @yield('content')
        <!-- END CONTENT -->
        @include('layouts.scripts')

        <!-- BEGIN FOOTER-->
        @include('layouts.footer')
        <!-- END FOOTER -->
    </body>
</html>

