<!-- BEGIN META -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta charset="utf-8">
<!-- Favicon icon -->
<link rel="shortcut icon" href="{{asset('assets/site/img/favicon.ico')}}" type="image/x-icon" />
<!-- END META -->
<!-- BEGIN STYLESHEETS -->

<link rel="stylesheet" type="text/css" href="{{asset('assets/site/css/animate.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/site/css/bootstrap-submenu.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/site/css/magnific-popup.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/site/css/map.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/site/css/jquery.mCustomScrollbar.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/site/css/dropzone.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/site/css/slick.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/site/css/style.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/site/css/skins/default.css')}}"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/site/fonts/flaticon/font/flaticon.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/site/fonts/linearicons/style.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/site/css/bootstrap.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,300,700"/>

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/site/css/ie10-viewport-bug-workaround.css')}}"/>
@yield('pageCss');
<!-- END STYLESHEETS -->