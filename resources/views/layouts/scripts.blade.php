<!-- BEGIN JAVASCRIPT -->

<script src="{{ asset('assets/site/js/jquery-2.2.0.min.js') }}"></script>
<script src="{{ asset('assets/site/js/popper.min.js') }}"></script>

<script src="{{ asset('assets/site/js/bootstrap-submenu.js') }}"></script>
<script src="{{ asset('assets/site/js/rangeslider.js') }}"></script>
<script src="{{ asset('assets/site/js/jquery.mb.YTPlayer.js') }}"></script>
<script src="{{ asset('assets/site/js/jquery.easing.1.3.js') }}"></script>
<script src="{{ asset('assets/site/js/jquery.scrollUp.js') }}"></script>
<script src="{{ asset('assets/site/js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
<script src="{{ asset('assets/site/js/dropzone.js') }}"></script>
<script src="{{ asset('assets/site/js/slick.min.js') }}"></script>
<script src="{{ asset('assets/site/js/jquery.filterizr.js') }}"></script>
<script src="{{ asset('assets/site/js/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('assets/site/js/app.js') }}"></script>
<script src="{{ asset('assets/site/js/bootstrap.min.js') }}"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="{{ asset('assets/site/js/ie10-viewport-bug-workaround.js') }}"></script>

@yield('pageScripts')

<!-- END JAVASCRIPT -->

