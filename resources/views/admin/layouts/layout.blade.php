<!DOCTYPE html>
<html lang="en">
<head>
    <title>Strongbuild - Admin panel</title>

    @include('admin.layouts.head')
    
</head>
<body >
    <!-- BEGIN HEADER-->
    @include('admin.layouts.header')
    <!-- END HEADER-->

    <!-- BEGIN BASE-->
    <div id="base">

            <!-- BEGIN SIDEBAR-->
            @include('admin.layouts.sidebar')
            <!-- END SIDEBAR-->

            <!-- BEGIN CONTENT-->
            <div id="content">
            @yield('content')
            </div><!--end #content-->
            <!-- END CONTENT -->

    </div><!--end #base-->
    <!-- END BASE -->
    @include('admin.layouts.scripts')

</body>
</html>