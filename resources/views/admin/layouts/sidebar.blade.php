<div id="sidebar" >
    <div class="sidebar-back" ></div>
    <div class="sidebar-content">
        <div class="nav-brand">
            <a class="main-brand" href=" <?php echo url('admin/home'); ?> ">
                <h3 class="text-light text-white"><span>Admin<strong>Panel</strong> </span><i class="fa fa-group  fa-fw"></i></h3>
            </a>
        </div>

        <!-- BEGIN MAIN MENU -->
        <ul class="main-menu">
            <!-- Menu Dashboard -->
            <li>
                <a href=" <?php echo url('admin/home'); ?> " <?php if($active=="dash") echo "class='active'";?> > 
                    <i class="fa fa-cogs  fa-fw"></i><span class="title">Dashboard</span>
                </a>
            </li><!--end /menu-item -->
            
            <!-- Menu Zgrade (Buildings) -->
            <li>
                <a href="javascript:void(0);" >
                    <i class="fa fa-building-o fa-fw" aria-hidden="true"></i><span class="title">Zgrade</span><span class="expand-sign">+</span>
                </a>
                <ul>
                    <li><a href="<?php echo url('admin/buildings'); ?>" <?php if($active == "allBuldings"){ echo "class='active'";}?> >Sve zgrade</a></li>
                    <li><a href="<?php echo url('admin/buildings/create'); ?>" <?php if($active == "addBuilding"){ echo "class='active'";}?> >Dodaj zgradu</a></li>
		</ul><!--end /submenu -->
            </li><!--end /menu-item -->   
            
            <!-- Menu Stanovi (Apartments) -->
            <li>
                <a href="javascript:void(0);"  >
                    <i class="fa fa-sitemap fa-fw"></i><span class="title">Stanovi</span><span class="expand-sign">+</span>
                </a>
                <ul>
                    <li><a href="<?php echo url('admin/apartments'); ?>" <?php if($active=="allApartments") echo "class='active'"; ?> >Stanovi</a></li>
                    <li><a href="<?php echo url('admin/apartments/create'); ?>" <?php if($active=="addApartment") echo "class='active'"; ?> >Dodaj stan</a></li>
		</ul><!--end /submenu -->
            </li><!--end /menu-item -->
            
             
        </ul><!--end .main-menu -->
        <!-- END MAIN MENU -->


    </div>
</div><!--end #sidebar-->

