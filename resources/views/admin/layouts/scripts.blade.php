<!-- BEGIN JAVASCRIPT -->

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script src="{{ asset('assets/js/libs/wysihtml5/jquery-1.8.2.min.js') }}"></script>
<script src="{{ asset('assets/js/libs/jquery/jquery-migrate-1.2.1.min.js') }}"></script>
<script src="{{ asset('assets/js/libs/jquery-ui/jquery-ui-1.10.3.custom.min.js') }}"></script>
<script src="{{ asset('assets/js/core/BootstrapFixed.js') }}"></script>
<script src="{{ asset('assets/js/libs/bootstrap/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/libs/spin.js/spin.min.js') }}"></script>
<script src="{{ asset('assets/js/libs/select2/select2.min.js') }}"></script>
<script src="{{ asset('assets/js/libs/multi-select/jquery.multi-select.js') }}"></script>
<script src="{{ asset('assets/js/libs/inputmask/jquery.inputmask.bundle.min.js') }}"></script>
<script src="{{ asset('assets/js/libs/moment/moment.min.js') }}"></script>
<script src="{{ asset('assets/js/libs/flot/jquery.flot.min.js') }}"></script>
<script src="{{ asset('assets/js/libs/flot/jquery.flot.time.min.js') }}"></script>
<script src="{{ asset('assets/js/libs/flot/jquery.flot.resize.min.js') }}"></script>
<script src="{{ asset('assets/js/libs/flot/jquery.flot.orderBars.js') }}"></script>
<script src="{{ asset('assets/js/libs/flot/jquery.flot.pie.js') }}"></script>
<script src="{{ asset('assets/js/libs/jquery-knob/jquery.knob.js') }}"></script>
<script src="{{ asset('assets/js/libs/sparkline/jquery.sparkline.min.js') }}"></script>
<script src="{{ asset('assets/js/libs/fullcalendar/fullcalendar.min.js') }}"></script>
<script src="{{ asset('assets/js/core/demo/DemoCharts.js') }}"></script>
<script src="{{ asset('assets/js/core/App.js') }}"></script>
<script src="{{ asset('assets/js/core/demo/Demo.js') }}"></script>
<script src="{{ asset('assets/js/libs/slimscroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('assets/js/libs/toastr/toastr.js') }}"></script>
<script src="{{ asset('assets/js/libs/typeahead/typeahead.min.js') }}"></script>
<script src="{{ asset('assets/js/libs/DataTables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/js/libs/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('assets/js/libs/ckeditor/adapters/jquery.js') }}"></script>
<script src="{{ asset('assets/js/libs/bootstrap-datetimepicker/bootstrap-datetimepicker.js') }}"></script>
<script src="{{ asset('assets/js/libs/bootstrap-datetimepicker/locales/bootstrap-datetimepicker.sr.js') }}"></script>
<script src="{{ asset('assets/js/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.js') }}"></script>
<script src="{{ asset('assets/js/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.js') }}"></script>
<script src "{{asset('assets/js/libs/jquery/jquery-1.11.0.min.js')}}"></script>
@yield('pageScripts')

<script src="{{ asset('assets/js/notifications.js') }}"></script>

<!-- END JAVASCRIPT -->
