<!-- BEGIN META -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="keywords" content="your,keywords">
<meta name="description" content="">
<!-- END META -->

<!-- BEGIN STYLESHEETS -->
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,300,400,600,700,800' rel='stylesheet' type='text/css'/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/boostbox.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/boostbox_responsive.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/font-awesome.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/libs/jquery-ui/jquery-ui-boostbox.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/libs/bootstrap-datetimepicker/bootstrap-datetimepicker.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/libs/bootstrap-colorpicker/bootstrap-colorpicker.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/libs/bootstrap-tagsinput/bootstrap-tagsinput.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/libs/wysihtml5/wysihtml5.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap-wysihtml5.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/libs/blueimp-file-upload/jquery.fileupload.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/libs/toastr/toastr.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/libs/typeahead/typeahead.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/libs/DataTables/jquery.dataTables.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/libs/DataTables/jquery.dataTables.css')}}"/>
 
@yield('pageCss');
<!-- END STYLESHEETS -->
