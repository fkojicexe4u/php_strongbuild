@extends('admin.layouts.layout')

@section('pageCss')
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/dashboardPower.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/theme-default/bootstrap.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/theme-default/jquery-ui-boostbox.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/libs/fullcalendar/fullcalendar.css')}}"/>

@stop

@section('content')

<section>
    <ol class="breadcrumb">
        <li><a href="../../html/.html">home</a></li>
        <li class="active">Profile</li>
    </ol>
    <div class="section-header">
        <h3 class="text-standard"><i class="fa fa-fw fa-arrow-circle-right text-gray-light"></i> Profile <small>Welcome {{ Auth::user()->name }}</small></h3>
    </div>
    <br>
    <div class="section-body">
        <!-- BEGIN CALENDAR-->
        <div class="col-lg-12">
            <div class=" box-tiles style-support3">
                <div class="row">
                    <div class=" bb col-md-2"> </div>
                    <div class="col-md-8 style-white">
                        <div id="myCalendar"></div>
                    </div><!--end .col-md-9 -->
                </div><!--end .row -->
            </div><!--end .box -->
        </div><!--end .col-lg-12 -->
        <!-- END CALENDAR-->
        <br>
    </div><!--end .section-body -->
</section>

@stop


@section('pageScripts')
<script src="{{ asset('assets/js/dashboard.js') }}"></script>
@stop
