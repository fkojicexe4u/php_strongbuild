<div class="form-group">
    @if(isset($apartment))
    <div class="form-group" dispaly="none">
        <input class="hidden" id="apartment_id" name="apartment_id" value="{{ $apartment->id }}"/>
    </div>
    @endif
    @if(isset($apartments_building))
    <div class="form-group" dispaly="none">
        <input id="building_id" name="building_id" class="hidden" value="{{ $apartments_building }}"/>
    </div>
    @else
    <div class = "form-group"><!-- Select building-->
        <div class="col-md-2">
            <label class="control-label">Zgrada:</label>
        </div>
        <div class="col-md-10">
             @if(isset($apartment)) <select name="building_id" id="building_id" class="form-control form-validate" placeholder="ime zgrade" required>
                 @foreach($buildings as $building)
                 <option value="{{ $building->id }}" <?php if($building->id == $apartment->building_id){ echo 'selected="selected"';} ?> >{{ $building->name }}</option>
                 @endforeach 
             </select>
             @else <select name="building_id" id="building_id" class="form-control form-validate" placeholder="ime zgrade"  required>
                 @foreach($buildings as $building)
                 <option value="{{ $building->id }}">{{ $building->name }}</option>
                 @endforeach
             </select> 
             @endif
        </div>
    </div><!-- end of building selection form field -->
    @endif
    <div class = "form-group"><!-- Add/edit bay -->
        <div class="col-md-2">
             <label class="control-label">Lamela:</label>
        </div>
        <div class="col-md-10" float="right"> 
            @if(isset($apartment)) <input type="text" name="bay" id = "bay" class="form-control"  value="{{ $apartment->bay }}" required data-rule-number="true"/>
            @else <input type="text" name="bay" id="bay" class="form-control form-validate" placeholder="lamela u kojoj je stan" required data-rule-number="true"/>
            @endif
        </div>
    </div><!-- end of "bay" form field -->
    <div class = "form-group"><!-- Add/edit apartment floor -->
        <div class="col-md-2">
             <label class="control-label">Sprat:</label>
        </div>
        <div class="col-md-10">
            @if(isset($apartment)) <input type="text" name="floor" id="floor" class="form-control" value="{{ $apartment->floor }}" required data-rule-number="true"/>
            @else <input type="text" name="floor" id="floor" class="form-control form-validate" placeholder="sprat na kome je stan" required data-rule-number="true"/>
            @endif
        </div>
    </div><!-- end of "floor" form field -->
    <div class = "form-group"><!-- Add/edit apartment number -->
        <div class="col-md-2"> 
             <label class="control-label">Broj:</label>
        </div>
        <div class="col-md-10">
            @if(isset($apartment)) <input type="text" name="number" id = "number" class="form-control" value="{{ $apartment->number }}" required data-rule-number="true"/>
            @else <input type="text" name="number" id = "number" class="form-control form-validate" placeholder="broj stana" requred data-rule-number="true"/>
            @endif
        </div>
    </div><!-- end of "aparment number" form field -->
    <div class = "form-group"><!-- Add/edit apartment structure -->
        <div class="col-md-2"> 
             <label class="control-label">Struktura:</label>
        </div>
        <div class="col-md-10">
            @if(isset($apartment)) <input type="text" name="structure" id = "structure" class="form-control" value="{{ $apartment->structure }}" required data-rule-number="true"/>
            @else <input type="text" name="structure" id = "structure" class="form-control form-validate" placeholder="struktura stana" requred data-rule-number="true"/>
            @endif
        </div>
    </div><!-- end of "structure" form field -->
    <div class = "form-group"><!-- Add/edit apartment size -->
        <div class="col-md-2"> 
             <label class="control-label">Veličina:</label>
        </div>
        <div class="col-md-10">
            @if(isset($apartment)) <input type="text" name="size" id = "size" class="form-control" value="{{ $apartment->size }}" required data-rule-number="true"/>
            @else <input type="text" name="size" id = "size" class="form-control form-validate" placeholder="veličina stana u metrima kvadratnim" requred data-rule-number="true"/>
            @endif
        </div>
    </div><!-- end of "size" form field -->
    <div class = "form-group"><!-- Add/edit apartment price -->
        <div class="col-md-2"> 
             <label class="control-label">Cena kvadrata:</label>
        </div>
        <div class="col-md-10">
            @if(isset($apartment)) <input type="text" name="unit_price" id = "unit_price" class="form-control" value="{{ $apartment->unit_price }}" required data-rule-number="true"/>
            @else <input type="text" name="unit_price" id = "unit_price" class="form-control form-validate" placeholder="cena kvadratnog metra" requred data-rule-number="true"/>
            @endif
        </div>
    </div><!-- end of "aparment number" form field -->
    <div class = "form-group"><!-- Should appartment show full price -->
        <div class="col-md-2"> 
             <label class="control-label">Prikazivanje pune cene:</label>
        </div>
        <div class="col-md-10">
            @if(isset($apartment)) <div data-toggle="buttons">
                    <label class="btn radio-inline btn-radio-success-inverse <?php if($apartment->showing_full_price == 1){echo 'active';} ?>">
			<input type="radio" name="showing_full_price" value="1" <?php if($apartment->showing_full_price == 1){echo 'checked';} ?>> Da
                    </label>
                    <label class="btn radio-inline btn-radio-danger-inverse <?php if($apartment->showing_full_price == 0){echo 'active';} ?>">
			<input type="radio" name="showing_full_price" value="0" <?php if($apartment->showing_full_price == 0){echo 'checked';} ?>> Ne
                    </label>
		</div>  
            @else <div data-toggle="buttons">
                    <label class="btn radio-inline btn-radio-success-inverse active">
			<input type="radio" name="showing_full_price" value="1" checked> Da
                    </label>
                    <label class="btn radio-inline btn-radio-danger-inverse">
			<input type="radio" name="showing_full_price" value="0"> Ne
                    </label>
		</div> 
            @endif
        </div>
    </div><!-- end of "full price" radio button field -->
    <div class = "form-group"><!-- Select status-->
        <div class="col-md-2">
            <label class="control-label">Status:</label>
        </div>
        <div class="col-md-10">
             @if(isset($apartment)) <select name="status_id" id ="status_id" class="form-control form-validate" placeholder="status" required>
                 @foreach($statuses as $status)
                 <option value="{{ $status->id }}" <?php if($status->id == $apartment->status_id){ echo 'selected="selected"';} ?> >{{ $status->name }}</option>
                 @endforeach 
                 </select>
             @else <select name="status_id" id ="status_id" class="form-control form-validate" required>
                 @foreach($statuses as $status)
                 <option value="{{ $status->id }}">{{ $status->name }}</option>
                 @endforeach
             </select> 
             @endif
        </div>
    </div><!-- end of status selection form field -->
    <div class="form-group"><!--Add plan of apartment -->
        <div class="col-md-2">
            <label class="control-label">Plan:</label>
        </div>
        <div class="col-md-10">
            <input type="button" id="uploadPlan" class="btn btn-primary" value="Izaberi plan" />
            <input type="file" name="plan" id="uploadPlanFile" class="uploadPlanFile" style="display: none;" required/>
            <input type="button" id="removePlan" class="btn btn-primary" value="Poništi izbor" disabled="disabled" />
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-2">
            <input type="hidden" name="plan_feed" id="planFeed" value="<?php if(isset($apartment)){echo $apartment->plan;} else{echo 'default.png';} ?>">
            <input type="hidden" name="plan_check" id="planCheck" value="">
        </div>
        <div class="col-md-10">
            <span id="planHolder">
            </span>
        </div>
    </div><!-- end of "plan" form field -->
    <div class="form-group"><!--Add first photo of apartment -->
        <div class="col-md-2">
            <label class="control-label">Prva slika:</label>
        </div>
        <div class="col-md-10">
            <input type="button" id="uploadPhoto1" class="btn btn-primary" value="Izaberi sliku" />
            <input type="file" name="image_one" id="uploadPhoto1File" class="uploadPhoto1File" style="display: none;" required/>
            <input type="button" id="removePhoto1" class="btn btn-primary" value="Poništi izbor" disabled="disabled" />
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-2">
            <input type="hidden" name="photo1_feed" id="photo1Feed" value="<?php if(isset($apartment)){echo $apartment->image_one;} else{echo 'default.png';} ?>">
            <input type="hidden" name="photo1_check" id="photo1Check" value="">
        </div>
        <div class="col-md-10">
            <span id="photo1Holder">
            </span>
        </div>
    </div><!-- end of "first photo" form field -->
    <div class="form-group"><!--Add second photo of apartment -->
        <div class="col-md-2">
            <label class="control-label">Druga slika:</label>
        </div>
        <div class="col-md-10">
            <input type="button" id="uploadPhoto2" class="btn btn-primary" value="Izaberi sliku" />
            <input type="file" name="image_two" id="uploadPhoto2File" class="uploadPhoto2File" style="display: none;" required/>
            <input type="button" id="removePhoto2" class="btn btn-primary" value="Poništi izbor" disabled="disabled" />
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-2">
            <input type="hidden" name="photo2_feed" id="photo2Feed" value="<?php if(isset($apartment)){echo $apartment->image_two;} else{echo 'default.png';} ?>" />
            <input type="hidden" name="photo2_check" id="photo2Check" value="" />
        </div>
        <div class="col-md-10">
            <span id="photo2Holder">
            </span>
        </div>
    </div><!-- end of "second photo" form field -->
    <div class="form-group"><!--Add third photo of apartment -->
        <div class="col-md-2">
            <label class="control-label">Treća slika:</label>
        </div>
        <div class="col-md-10">
            <input type="button" id="uploadPhoto3" class="btn btn-primary" value="Izaberi sliku" />
            <input type="file" name="image_three" id="uploadPhoto3File" class="uploadPhoto3File" style="display: none;" required/>
            <input type="button" id="removePhoto3" class="btn btn-primary" value="Poništi izbor" disabled="disabled" />
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-2">
            <input type="hidden" name="photo3_feed" id="photo3Feed" value="<?php if(isset($apartment)){echo $apartment->image_three;} else{echo 'default.png';} ?>" />
            <input type="hidden" name="photo3_check" id="photo3Check" value="" />
        </div>
        <div class="col-md-10">
            <span id="photo3Holder">
            </span>
        </div>
    </div><!-- end of "third photo" form field -->
    <div class="form-group"><!--Add fourth photo of apartment -->
        <div class="col-md-2">
            <label class="control-label">Četvrta slika:</label>
        </div>
        <div class="col-md-10">
            <input type="button" id="uploadPhoto4" class="btn btn-primary" value="Izaberi sliku" />
            <input type="file" name="image_four" id="uploadPhoto4File" class="uploadPhoto4File" style="display: none;" required/>
            <input type="button" id="removePhoto4" class="btn btn-primary" value="Poništi izbor" disabled="disabled" />
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-2">
            <input type="hidden" name="photo4_feed" id="photo4Feed" value="<?php if(isset($apartment)){echo $apartment->image_four;} else{echo 'default.png';} ?>" />
            <input type="hidden" name="photo4_check" id="photo4Check" value="" />
        </div>
        <div class="col-md-10">
            <span id="photo4Holder">
            </span>
        </div>
    </div><!-- end of "fourth photo" form field -->
    <div class="form-group"><!--Add fifth photo of apartment -->
        <div class="col-md-2">
            <label class="control-label">Peta slika:</label>
        </div>
        <div class="col-md-10">
            <input type="button" id="uploadPhoto5" class="btn btn-primary" value="Izaberi sliku" />
            <input type="file" name="image_five" id="uploadPhoto5File" class="uploadPhoto5File" style="display: none;" required/>
            <input type="button" id="removePhoto5" class="btn btn-primary" value="Poništi izbor" disabled="disabled" />
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-2">
            <input type="hidden" name="photo5_feed" id="photo5Feed" value="<?php if(isset($apartment)){echo $apartment->image_five;} else{echo 'default.png';} ?>" />
            <input type="hidden" name="photo5_check" id="photo5Check" value="" />
        </div>
        <div class="col-md-10">
            <span id="photo5Holder">
            </span>
        </div>
    </div><!-- end of "fifth photo" form field -->
</div><!-- end of form group -->
<div class="form-group">
    <div class="form-footer col-lg-offset-1 col-md-offset-1 col-sm-9">
        <button type="button" class="btn btn-primary" id="addApartment" data-toggle="modal" data-target="#simpleModal">{{$submit}} Stan</button>
    </div>
</div>
<!-- START SIMPLE MODAL MARKUP -->
<div class="modal fade" id="simpleModal" tabindex="-1" role="dialog" aria-labelledby="simpleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="simpleModalLabel">Sačuvajte promene</h4>
            </div>
            <div class="modal-body">
                <p><text>Da li želite da </text> @if ($submit == 'Dodaj')<text>dodate</text>
                                   @elseif ($submit == 'Uredi')<text>uredite</text>
                                   @endif <text>ovaj stan?</text>
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Ne</button>
                <button type="submit" id="submitApartment" class="btn btn-primary">Sačuvaj</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- END SIMPLE MODAL MARKUP -->




