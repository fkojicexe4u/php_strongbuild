<div class="form-group">
    @if(isset($building))
    <div class="form-group" display="none">
        <input class="hidden" name="building_id" id="building_id" value="{{ $building->id }}">
    </div>
    @endif
    <div class = "form-group"><!-- Add/edit building name-->
        <div class="col-md-2">
            <label class="control-label">Ime:</label>
        </div>
        <div class="col-md-10">
             @if(isset($building)) <input type="text" name="name" id = "name" class="form-control" placeholder="name" value="{{ $building->name }}" required></input>                       
             @else <input type="text" name="name" id ="name" class="form-control form-validate" placeholder="ime zgrade" required></input> 
             @endif
        </div>
    </div><!-- end of "name" form field -->
    <div class = "form-group"><!-- Add/edit buiding adress -->
        <div class="col-md-2">
             <label class="control-label">Adresa:</label>
        </div>
        <div class="col-md-10" float="right"> 
            @if(isset($building)) <input type="text" name="adress" id = "adress" class="form-control" value="{{ $building->adress }}" required></input> 
            @else <input type="text" name="adress" id = "adress" class="form-control form-validate" placeholder="adresa zgrade" required></input> 
            @endif
        </div>
    </div><!-- end of "adress" form field -->
    <div class = "form-group"><!-- Add/edit building municipality -->
        <div class="col-md-2">
             <label class="control-label">Opština:</label>
        </div>
        <div class="col-md-10">
            @if(isset($building)) <input type="text" name="municipality" id = "municipality" class="form-control" value="{{ $building->municipality }}" required></input> 
            @else <input type="text" name="municipality" id = "municipality" class="form-control form-validate" placeholder="opština u kojoj je zgrada" required></input> 
            @endif
        </div>
    </div><!-- end of "municipality" form field -->
    <div class = "form-group"><!-- Add/edit building city -->
        <div class="col-md-2"> 
             <label class="control-label">Grad:</label>
        </div>
        <div class="col-md-10">
            @if(isset($building)) <input type="text" name="city" id = "city" class="form-control" value="{{ $building->city }}" required></input> 
            @else <input type="text" name="city" id = "city" class="form-control form-validate" placeholder="grad u kome je zgrada" requred></input> 
            @endif
        </div>
    </div><!-- end of "city" form field -->
    <div class = "form-group"><!-- Add/edit building built date -->
        <div class="col-md-2">
             <label class="control-label">Datum izgradnje:</label>
        </div>
        <div class="col-md-10">
            @if(isset($building)) <div class="input-group control-width-normal" id="my-date" data-date-format="DD. MMMM YYYY" lang="sr" >
                                    <input type="text" class="form-control" name="built_date" value="{{ $building->built_date }}"/>
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                  </div> 
            @else <div class="input-group control-width-normal" id="my-date" data-date-format="DD. MMMM YYYY" lang="sr">
                    <input type="text" class="form-control" name="built_date" />
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                  </div> 
            @endif
        </div>
    </div><!-- end of "built date" form field -->
    <div class = "form-group"><!-- Add/edit building number of bays -->
        <div class="col-md-2">
             <label class="control-label">Broj lamela:</label>
        </div>
        <div class="col-md-10" float="right">
            @if(isset($building)) <input type="text" name="bays_number" id = "bays_number" class="form-control" value="{{ $building->bays_number }}" data-rule-number="true"></input> 
            @else <input type="text" name="bays_number" id = "bays_number" class="form-control form-validate" placeholder="broj lamela" data-rule-number="true"></input> 
            @endif
        </div>
    </div><!-- end of "number of bays" form field -->
    <div class = "form-group"><!-- Add/edit building number of floors -->
        <div class="col-md-2">
             <label class="control-label">Spratnost:</label>
        </div>
        <div class="col-md-10">
            @if(isset($building)) <input type="text" name="floors_number" id = "floors_number" class="form-control" value="{{ $building->floors_number }}" required data-rule-number="true"></input> 
            @else <input type="text" name="floors_number" id = "floors_number" class="form-control form-validate" placeholder="broj spratova" required data-rule-number="true"></input> 
            @endif
        </div>
    </div><!-- end of "number of floors" form field -->
    <div class="form-group"><!--Add main image -->
        <div class="col-md-2">
            <label class="control-label">Naslovna slika:</label>
        </div>
        <div class="col-md-10">
            <input type="button" id="uploadMainImage" class="btn btn-primary" value="Izaberi sliku" />
            <input type="file" name="main_image" id="uploadMainImageFile" class="uploadMainImageFile" style="display: none;" required />
            <input type="button" id="removeMainImage" class="btn btn-primary" value="Poništi izbor" disabled="disabled" />
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-2">
            <input type="hidden" name="main_image_feed" id="mainImageFeed" value="<?php if(isset($building)){echo $main_image;} else{echo 'default.png';} ?>">
            <input type="hidden" name="main_image_check" id="mainImageCheck" value="">
        </div>
        <div class="col-md-10">
            <span id="mainImageHolder">
            </span>
        </div>
    </div><!-- end of "main image" form field -->
    <div class="form-group"><!--Add plans of building -->
        <div class="col-md-2">
            <label class="control-label">Planovi:</label>
        </div>
        <div class="col-md-10">
            <input type="button" id="uploadPlans" class="btn btn-primary" value="Izaberi planove..." />
            <input type="file" name="plans[]" id="uploadPlanFiles" class="uploadPlanFiles" multiple style="display: none;" />
            <input type="button" id="removePlans" class="btn btn-primary" value="Poništi izbor" disabled="disabled" />
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-2">
            <input type="hidden" name ="plans_feed" id="plansFeed" value="<?php if(isset($building)&&$plans_feed!=""){echo $plans_feed;} else{echo 'default.png';} ?>">
            <input type="hidden" name="plans_check" id="plansCheck" value="">
        </div>
        <div class="col-md-10">
            <span id="plansHolder">
            </span>
        </div>
    </div><!-- end of "plans" form field -->
    <div class="form-group"><!--Add photos of building -->
        <div class="col-md-2">
            <label class="control-label">Slike:</label>
        </div>
        <div class="col-md-10">
            <input type="button" id="uploadPhotos" class="btn btn-primary" value="Izaberi slike..." />
            <input type="file" name="photos[]" id="uploadPhotoFiles" class="uploadPhotoFiles" multiple style="display: none;" />
            <input type="button" id="removePhotos" class="btn btn-primary" value="Poništi izbor" disabled="disabled" />
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-2">
            <input type="hidden" name="photos_feed" id="photosFeed" value="<?php if(isset($building)&&$photos_feed!=""){echo $photos_feed;} else{echo 'default.png';} ?>">
            <input type="hidden" name="photos_check" id="photosCheck" value="">
        </div>
        <div class="col-md-10">
            <span id="photosHolder">
            </span>
        </div>
    </div><!-- end of "photos" form field -->

</div><!-- end of form group -->
<div class="form-group">
    <div class="form-footer col-lg-offset-1 col-md-offset-1 col-sm-9">
        <button type="button" class="btn btn-primary" id="addBuilding" data-toggle="modal" data-target="#simpleModal">{{$submit}} Zgradu</button>
    </div>
</div>
<!-- START SIMPLE MODAL MARKUP -->
<div class="modal fade" id="simpleModal" tabindex="-1" role="dialog" aria-labelledby="simpleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="simpleModalLabel">Sačuvajte promene</h4>
            </div>
            <div class="modal-body">
                <p><text>Da li želite da </text> @if ($submit == 'Dodaj')<text>dodate</text>
                                   @elseif ($submit == 'Uredi')<text>uredite</text>
                                   @endif <text>ovu zgradu?</text>
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Ne</button>
                <button type="submit" id="submitBuilding" class="btn btn-primary">Sačuvaj</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- END SIMPLE MODAL MARKUP -->


 