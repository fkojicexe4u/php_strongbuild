<div class="form-group">
    <div class = "form-group-sm" display = "none"><!-- Hidden filed for id (building, photo, or main image)-->
        <div class="col-md-10">
            <input type="hidden" name="building_id" id = "defaultName" class="form-control" value="{{ $building_id }}"> </input> 
        </div>
    </div><!-- end of "building id" field -->
    <div class = "form-group-sm" display = "none"><!-- Hidden filed for photo type -->
        <div class="col-md-10">
            <input type="hidden" name="photo_type" id = "defaultName" class="form-control" value="{{ $photo_type }}"> </input> 
        </div>
    </div><!-- end of "photo type" field -->
    <div class = "form-group-sm" display = "none"><!-- Hidden filed for photo type -->
        <div class="col-md-10" float="right">
            <input type="hidden" name="id" id = "defaultName" class="form-control" value="{{ $id }}"> </input> 
        </div>
    </div><!-- end of "photo type" field -->
    <div class="form-group"><!--Add main image -->
        <div class="col-md-2">
            <label class="control-label">{{ ucfirst($photo_type) }}:</label>
        </div>
        <div class="col-md-10">
            <input id="uploadMainImage" type="button" value="Izaberi sliku" class="btn btn-primary" />
            <input id="uploadMainImageFile" class="uploadMainImageFile" name="main_image" type="file" style="display: none;" />
            <input id="removeMainImage" type="button" value="Poništi izbor" class="btn btn-primary" disabled="disabled" />
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-2">
            <input type="hidden" id="mainImageFeed" name="main_image_feed" value="<?php if(isset($feed)){echo $feed;} else{echo 'default.png';} ?>">
            <input type="hidden" id="mainImageCheck" name="main_image_check" value="">
        </div>
        <div class="col-md-10">
            <span id="mainImageHolder">
            </span>
        </div>
    </div><!-- end of "main image" form field -->
</div><!-- end of form group -->
<div class="form-group">
    <div class="form-footer col-lg-offset-1 col-md-offset-1 col-sm-9">
        <button type="button" class="btn btn-primary" id="addBuildingPhoto" data-toggle="modal" data-target="#simpleModal">{{ $submit }}: "{{ $photo_type }}"</button>
    </div>
</div>
<!-- START SIMPLE MODAL MARKUP -->
<div class="modal fade" id="simpleModal" tabindex="-1" role="dialog" aria-labelledby="simpleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="simpleModalLabel">Sačuvajte promene</h4>
            </div>
            <div class="modal-body">
                <p><text>Da li želite da </text>@if($submit == 'Dodaj') <text>dodate</text>
                                   @elseif($submit == 'Promeni') <text>promenite</text> @endif<text> ovu sliku koja je postavljana kao {{ $photo_type}}</text>
                                   @if($photo_type == 'naslovna')<text> slika</text>@endif</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Ne</button>
                <button type="submit" id="submitBuildingPhoto" class="btn btn-primary">Sačuvaj</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- END SIMPLE MODAL MARKUP -->
