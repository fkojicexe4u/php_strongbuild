@extends('admin.layouts.layout')

@section('content')
@if(Session::has('message'))
<input id="message" type="hidden" value="{{ Session::get('message') }}" />
@endif

<section>
    <ol class="breadcrumb">
        <li><a href="<?php echo url('admin/home'); ?>">Admin</a></li>
        <li class="active">Stanovi</li>
    </ol>
    <div class="section-header">
        <h3 class="text-standard"><i class="fa fa-fw fa-arrow-circle-right text-gray-light"></i> Stanovi <small>pregled</small></h3>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-outlined">
                    <div class="box-head">
                        <header><h4 class="text-light">Tabela sa svim <strong>stanovima</strong></h4></header>
                    </div>
                    <div id="apartmentTable" class="box-body table-responsive">
                        {!! Form::open(array('method' => 'DELETE', 'id' => 'apartmentForm', 'role' => 'form')) !!}
                        {!! Form::submit(null, ['id' => 'apartmentButton', 'class' => 'btn btn-primary createEditButton', 'style' => 'display: none;']) !!}
                        {!! Form::close() !!}
                        <table id="datatable" class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th>&nbsp;&nbsp;&nbsp;#</th>
                                <th>Stan broj:</th>
                                <th>Zgrada</th>
                                <th>Lamela</th>
                                <th>Sprat</th>
                                <th>Struktura</th>
                                <th>Veličina</th>
                                <th>Cena po kvadratu</th>
                                <th>Prikazivanje pune cene</th>
                                <th style="min-width: 135px">Status</th>
                                <th style="min-width: 85px">Akcije</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($apartments as $num => $apartment)
                               <tr id="{{ $apartment->id }}" class="gradeX">
                                <td>&nbsp;&nbsp;&nbsp;{{ $num + 1 }}</td>
                                <td><a href="{{ URL('/admin/apartments/' . $apartment->id.'/edit/')}}">{{ $apartment->number }}</a></td>
                                <td>{{ $apartment->building_id }}</td>
                                <td>{{ $apartment->bay }}</td>
                                <td>{{ $apartment->floor }}</td>
                                <td>{{ $apartment->structure }}</td>
                                <td>{{ $apartment->size }}</td>
                                <td>{{ $apartment->unit_price }}</td>
                                <td>@if($apartment->showing_full_price) da @else ne @endif</td>
                                <td>{{ $apartment->status_id }}  
                                    @if($apartment->status_id != 'Slobodan')
                                        <button type="button" class="btn btn-xs btn-success btn-equal status1Apartment" data-toggle="tooltip" data-placement="top" data-original-title="Promeni u 'Slobodan'"><i class="fa fa-check"></i></button>
                                    @endif
                                    @if($apartment->status_id != 'Rezervisan')
                                        <button type="button" class="btn btn-xs btn-warning btn-equal status2Apartment" data-toggle="tooltip" data-placement="top" data-original-title="Promeni u 'Rezervisan'"><i class="fa fa-warning"></i></button>
                                    @endif
                                    @if($apartment->status_id != 'Prodat')
                                        <button type="button" class="btn btn-xs btn-danger btn-equal status3Apartment" data-toggle="tooltip" data-placement="top" data-original-title="Promeni u 'Prodat'"><i class="fa fa-circle"></i></button>
                                    @endif
                                </td>
                                <td>
                                    <button type="button" class="btn btn-xs btn-inverse btn-equal photoApartment" data-toggle="tooltip" data-placement="top" data-original-title="Fotografije stana"><i class="fa fa-camera"></i></button>
                                    <button type="button" class="btn btn-xs btn-info btn-equal cloneApartment" data-toggle="tooltip" data-placement="top" data-original-title="Kloniranje stana"><i class="fa fa-copy"></i></button>
                                    <button type="button" class="btn btn-xs btn-danger btn-equal deleteApartment" data-toggle="tooltip" data-placement="top" data-original-title="Brisanje stana"><i class="fa fa-trash-o"></i></button>
                                </td>
                            </tr> 
                            @endforeach
                            </tbody>
                        </table>
                    </div><!--end .box-body -->
                </div><!--end .box -->
            </div><!--end .col-lg-12 -->
        </div>
        <!-- END STRIPED TABLE -->
    </div>
    </div>
</section>
@stop

@section('pageScripts')
<script src="{{ asset('/assets/js/datatable.js') }}"></script>
<script src="{{ asset('/assets/js/apartments.js') }}"></script>

@stop
