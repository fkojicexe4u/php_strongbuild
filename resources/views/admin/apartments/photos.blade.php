@extends('admin.layouts.layout')

@section('content')
@if(Session::has('message'))
<input id="message" type="hidden" value="{{ Session::get('message') }}" />
@endif

<section>
    <ol class="breadcrumb">
        <li><a href="<?php echo url('home'); ?>">Admin</a></li>
        <li class="active">Slike stana {{$apartment->number}} u zgradi {{$apartment->building_id}} lamela {{$apartment->bay}}</li>
    </ol>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-outlined">
                    <div class="box-head">
                        <header><h4 class="text-light">Slike<strong> stana</strong></h4></header>
                    </div>
                    <div class="box box-outlined">
                        <div class="box-head">
                            <header><h4 class="text-light">Plan:</h4></header>
                        </div>
                        <div class = "box box-body">
                            <img class = "center-block" id = "plan" src = "{{ asset('images/apartments/'.$apartment->plan) }}">
                        </div>
                    </div>
                    <div class="box box-outlined">
                        <div class="box-head">
                            <header><h4 class="text-light">Slika 1:</h4></header>
                        </div>
                        <div class = "box box-body">
                            <img class = "center-block" id = "image_one" src = "{{ asset('images/apartments/'.$apartment->image_one) }}">
                        </div>
                    </div>
                    <div class="box box-outlined">
                        <div class="box-head">
                            <header><h4 class="text-light">Slika 2:</h4></header>
                        </div>
                        <div class = "box box-body">
                            <img class = "center-block" id = "image_two" src = "{{ asset('images/apartments/'.$apartment->image_two) }}">
                        </div>
                    </div>
                    <div class="box box-outlined">
                        <div class="box-head">
                            <header><h4 class="text-light">Slika 3:</h4></header>
                        </div>
                        <div class = "box box-body">
                            <img class = "center-block" id = "image_three" src = "{{ asset('images/apartments/'.$apartment->image_three) }}">
                        </div>
                    </div>
                    <div class="box box-outlined">
                        <div class="box-head">
                            <header><h4 class="text-light">Slika 4:</h4></header>
                        </div>
                        <div class = "box box-body">
                            <img class = "center-block" id = "image_four" src = "{{ asset('images/apartments/'.$apartment->image_four) }}">
                        </div>
                    </div>
                    <div class="box box-outlined">
                        <div class="box-head">
                            <header><h4 class="text-light">Slika 5:</h4></header>
                        </div>
                        <div class = "box box-body">
                            <img class = "center-block" id = "image_five" src = "{{ asset('images/apartments/'.$apartment->image_five) }}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop
