@extends('admin.layouts.layout')

@section('content')
@if(Session::has('message'))
<input id="message" type="hidden" value="{{ Session::get('message') }}" />
@endif

<section>
    <ol class="breadcrumb">
        <li><a href="<?php echo url('home'); ?>">Admin</a></li>
        <li class="active">Slike zgrade {{$building->name}}</li>
    </ol>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-outlined">
                    <div class="box-head">
                        <header><h4 class="text-light">Sve slike zgrade <strong>{{ $building->name}}</strong></h4></header>
                    </div>
                    <div class="box box-outlined">
                        <div class="box-head">
                            <header><h4 class="text-light">Naslovna fotografija:</h4></header>
                        </div>
                        <div class = "box box-body">
                            <img class = "center-block" id = "mainPhoto" src = "{{ $main_image_path }}">
                        </div>
                        <div class = "box-footer">
                            <a class="btn btn-support3" href = "{{ URL('admin/buildings/changePhoto/naslovna_'.$building->id) }}">Promeni naslovnu sliku</a>
                        </div>
                    </div>
                    <div class="box box-outlined">
                        <div class="box-head">
                            <header><h4 class="text-light">Planovi:</h4></header>
                        </div>
                        <div class = "box box-body">
                            @if (!empty($plans))
                            @foreach($plans as $plan)
                            <div class="box box-outlined">
                                <img class = "center-block" id = "planPhoto{{ $plan->photo_id }}" src = "{{ $plan_path[$plan->id] }} ">
                                <br>
                                <a class="btn btn-support3" href = "{{ URL('admin/buildings/changePhoto/'.$plan->id) }}">Promeni plan</a>
                                <a  class="btn btn-support3" href = "{{ URL('admin/buildings/deletePhoto/'.$plan->id) }}">Obriši plan</a>
                                @endforeach
                            </div>
                            @else
                            <div class="box box-outlined">
                                <text>Trenutno nema unetih planova za ovu zgradu.</text>
                            </div>
                            @endif
                            
                        </div>
                        <div class = "box-footer">
                            <a  class="btn btn-support3" href = "{{ URL('admin/buildings/addPhoto/plan_'.$building->id) }}">Dodaj plan</a>
                        </div>
                    </div>
                    <div class="box box-outlined">
                        <div class="box-head">
                            <header><h4 class="text-light">Slike:</h4></header>
                        </div>
                        <div class = "box box-body">
                            @if (!empty($photos))
                            @foreach($photos as $photo)
                            <div class="box box-outlined">
                                <img class = "center-block" id = "slikaPhoto{{ $photo->photo_id }}" src = "{{ $photo_path[$photo->id] }} ">
                                <br>
                                <a class="btn btn-support3" href = "{{ URL('admin/buildings/changePhoto/'.$photo->id) }}">Promeni sliku</a>
                                <a class="btn btn-support3" href = "{{ URL('admin/buildings/deletePhoto/'.$photo->id) }}">Obriši sliku</a>
                            @endforeach
                            </div>
                            @else
                            <div class="box box-outlined">
                                <text>Trenutno nema unetih slika za ovu zgradu.</text>
                            </div>
                            @endif
                        </div>
                        <div class = "box-footer">
                            <a class="btn btn-support3" href = "{{ URL('admin/buildings/addPhoto/slika_'.$building->id) }}">Dodaj sliku</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop