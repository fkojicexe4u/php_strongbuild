@extends('admin.layouts.layout')

@section('content')
@if(Session::has('message'))
<input id="message" type="hidden" value="{{ Session::get('message') }}" />
@endif

<section>
    <ol class="breadcrumb">
        <li><a href="<?php echo url('admin/home'); ?>">Admin</a></li>
        <li class="active">Zgrade</li>
    </ol>
    <div class="section-header">
        <h3 class="text-standard"><i class="fa fa-fw fa-arrow-circle-right text-gray-light"></i> Zgrade <small>Dodaj/Uredi</small></h3>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-outlined">
                    <div class="box-head">
                        <header><h4 class="text-light">Tabela sa svim <strong>Zgradama</strong></h4></header>
                    </div>
                    <div id="buildingTable" class="box-body table-responsive">
                        {!! Form::open(array('method' => 'DELETE', 'id' => 'buildingForm', 'role' => 'form')) !!}
                        {!! Form::submit(null, ['id' => 'buildingButton', 'class' => 'btn btn-primary createEditButton', 'style' => 'display: none;']) !!}
                        {!! Form::close() !!}    
                        <table id="datatable" class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th>&nbsp;&nbsp;&nbsp;#</th>
                                <th>Ime</th>
                                <th>Adresa</th>
                                <th>Opština</th>
                                <th>Grad</th>
                                <th>Izgrađena</th>
                                <th>Lamela</th>
                                <th>Spratova</th>
                                <th style="min-width: 85px">Akcije</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($buildings as $num => $building)
                               <tr id="{{ $building->id }}" class="gradeX">
                                <td>&nbsp;&nbsp;&nbsp;{{ $num + 1 }}</td>
                                <td><a href="{{ URL('/admin/buildings/' . $building->id.'/edit/')}}">{{ $building->name }}</a></td>
                                <td>{{ $building->adress }}</td>
                                <td>{{ $building->municipality }}</td>
                                <td>{{ $building->city }}</td>
                                <td>{{ $building->built_date }}</td>
                                <td>{{ $building->bays_number }}</td>
                                <td>{{ $building->floors_number }}</td>
                                <td>
                                    <button type="button" class="btn btn-xs btn-inverse btn-equal photoBuilding" data-toggle="tooltip" data-placement="top" data-original-title="Fotografije zgrade"><i class="fa fa-camera"></i></button>
                                    <button type="button" class="btn btn-xs btn-danger btn-equal deleteBuilding" data-toggle="tooltip" data-placement="top" data-original-title="Brisanje zgrade"><i class="fa fa-trash-o"></i></button>
                                    
                                </td>
                            </tr> 
                            @endforeach
                            </tbody>
                        </table>
                    </div><!--end .box-body -->
                </div><!--end .box -->
            </div><!--end .col-lg-12 -->
        </div>
        <!-- END STRIPED TABLE -->
    </div>
    </div>
</section>
@stop

@section('pageScripts')
<script src="{{ asset('/assets/js/datatable.js') }}"></script>
<script src="{{ asset('/assets/js/buildings.js') }}"></script>
@stop