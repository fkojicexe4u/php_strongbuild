@extends('admin.layouts.layout')

@section('pageCss')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/libs/select2/select2.css')}}"/>
@stop
    

@section('content')
<section>
    <ol class="breadcrumb">
        <li><a href="<?php echo url('admin/home'); ?>">Admin</a></li>
        <li class="active">Promeni sliku</li>
    </ol>
    <div class="section-header">
        <h3 class="text-standard"><i class="fa fa-fw fa-arrow-circle-right text-gray-light"></i> Forma za dodavanje slika zgrade</h3>
    </div>
    
    <!-- START HORIZONTAL BORDERED FORM -->
            <div class="col-lg-12">
                <div class="box box-outlined">
                    <div class="box-head">
                        <header><h4 class="text-light">Promeni <strong>sliku</strong></h4></header>
                    </div>
                    <div class="box-body no-padding">
                        {!! Form::open(array('url' => 'admin/buildings/updatePhoto', 'class' => 'form-horizontal form-bordered form-validate', 'role' => 'form', 'novalidate' => 'novalidate', 'files' => true)) !!}
                        @include('admin.forms.buildingphoto', ['submit' => 'Promeni'])
                        {!! Form::close() !!}
                        @include('admin.forms.error')
                    </div>
                </div>
            </div>
            <!-- END HORIZONTAL BORDERED FORM -->
</section>
@stop


@section('pageScripts')
<script src="{{ asset('/assets/js/datatable.js') }}"></script>
<script src="{{ asset('/assets/js/buildings.js') }}"></script>
@stop
